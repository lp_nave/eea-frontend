import React, { Component } from 'react'
import { Grid, Segment, Card, Message, Icon, Header, Dimmer, Loader, TransitionablePortal, Button } from 'semantic-ui-react'
import TopNav from '../components/TopNav'
import Searchbar from '../components/Searchbar'
import Jobcard from '../components/Job/Jobcard'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { JobActions } from '../actions'
import JobDataModal from '../components/Job/JobDataModal'

export class Jobs extends Component {

    constructor(props){
        super(props);
        this.state={
            data:null,
            id:null,
            open:false,
        }
    }

    componentDidMount(){
        if(this.props.location.search===""){
            this.props.jobActions.getAllJobs()
        }else{
            let preSearchQuery= this.props.location.search.substring(5);
            let obj={
                searchkey:null,
                location:preSearchQuery
            }
            this.props.jobActions.searchJobs(obj)
            debugger
        }

    }

    static getDerivedStateFromProps(nextProps,prevState){
        if(nextProps.jobData){
            return{
                data: nextProps.jobData

            }
            
        }if(nextProps.status!==null){
            if(nextProps.status===true){
                return{
                    status:"success",
                    openPortal:true
                }
            }else if(nextProps.status===false){
                return{
                    status:"fail",
                    openPortal:true
                }
            }
            else return null
        }
        else return null

    }

    handleRefresh=()=>{
        this.props.jobActions.getAllJobs()
    }

    openModal=()=>{
        debugger
        this.setState({
            open:true, 
            // id:id
        })
    }
    selected=(id)=>{
        this.setState({id:id, open:true})
    }

    closeModal=()=>{
        this.setState({open:false, id:null})
        this.props.jobActions.clearJob()
    }

    handleClose=()=>{
        
        this.setState({openPortal:false, status:null})
        this.props.jobActions.clearJob()
        // this.props.close()
    }

    render() {
        return (
            <div>
                <TopNav/>
                <div style={{ marginBottom:'2em', marginTop: '4em', marginLeft:'2em', marginRight:'2em', maxHeight:'100%' }}>
                    <Searchbar job type={'jobs'} refresh={this.handleRefresh}/>
                    <Segment>
                        <Grid>
                                <Grid.Column width={16}>
                                    <div style={{minHeight:'500px', maxHeight:'500px', overflowX:'hidden', overflowY:'auto'}}>
                                        <Header color='orange' size='large'>Jobs</Header>
                                        <Card.Group centered itemsPerRow={4}>
                                            {this.props.jobData!==null?
                                                <>
                                                {this.state.data.map((item, index)=>{
                                                    return <Jobcard key={index} data={item} selected={this.selected}/>
                                                })}
                                                </>
                                            :
                                            <Dimmer active>
                                                <Loader>Loading</Loader>
                                            </Dimmer>
                                            }
                                            
                                        </Card.Group>
                                    </div>
                                </Grid.Column>
                                <Grid.Column width={5}>

                                </Grid.Column>

                        </Grid>
                    </Segment>

                </div>
                <JobDataModal id={this.state.id} open={this.state.open} close={this.closeModal}/>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch){
    return{
        jobActions: bindActionCreators(JobActions,dispatch),
        // companyActions: bindActionCreators(CompanyActions,dispatch),
    }
}

function mapStateToProps(state){
    return {
        jobData:state.Jobs.jobs,
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Jobs))
