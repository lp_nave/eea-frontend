import React, { Component } from 'react'
import TopNav from '../../components/TopNav'
import UserProfileHeader from '../../components/User/UserProfileHeader'
import { Grid, Button, Segment, Container, Header, Card, Message, Icon } from 'semantic-ui-react'
import Vacancy, { Jobcard } from '../../components/Job/Jobcard'
import { connect } from 'react-redux'
import {UserProfileActions, CVActions, JobActions} from '../../actions'
import { withRouter } from 'react-router-dom'
import EditProfileModal from '../../components/User/EditProfileModal'
import UploadCVModel from '../../components/User/UploadCVModel'
import { bindActionCreators } from 'redux'
import JobDataModal from '../../components/Job/JobDataModal'


export class UserProfile extends Component {

    constructor(props){
        super(props);
        this.state={
            openEdit:false,

            firstName:'',
            lastName:'',
            email:'',
            image:null,
            imagePath:null,
            jobList:[],
            open:false
        }
    }

    componentDidMount(){
        console.log("props", this.props)
        this.props.UserProfileActions.getUserProfile();
        
    }

    static getDerivedStateFromProps(nextProps,prevState){
        if(nextProps.profileData){
            console.log("profile data", nextProps.profileData)
            return{
                firstName:nextProps.profileData.firstName,
                lastName: nextProps.profileData.lastName,
                email:nextProps.profileData.email,
                jobList: nextProps.profileData.joblist,
                image:nextProps.profileData.image,
                imagePath:nextProps.profileData.imagePath
            }
        }
        else{
            return null
        }
    }
    downloadYourCV=()=>{
        let email=localStorage.getItem("email")
        this.props.CVActions.downloadCV(email)
    }

    handleEditProfileBtn=()=>{
        this.setState({openEdit:true})
    }

    handleEditModalClose=()=>{
        this.setState({openEdit:false})
    }

    closeUpload=(e)=>{
        this.setState({open:false})
    }

    openUpload=(e)=>{
       this.setState({open:true})
    }

    selected=(id)=>{
        this.setState({id:id, openModal:true})
    }

    closeModal=()=>{
        this.setState({openModal:false, id:null})
        this.props.jobActions.clearJob()
    }


    render() {
        return (
            <div>
                <TopNav/>
                <Grid textAlign='center'>
                    <UserProfileHeader
                    firstName={this.state.firstName}
                    lastName={this.state.lastName} 
                    email={this.state.email}
                    jobSize={this.state.jobList.length}
                    image={this.state.image}
                    imagePath={this.state.imagePath}
                    />
                    <Container style={{marginTop:'1em'}}>
                        <Segment>
                            <Button  color='black' onClick={this.downloadYourCV}> <Icon name='download' /> Your CV</Button>
                            <Button color='orange'onClick={this.openUpload}><Icon name='upload'/> Upload CV</Button>
                            <Button color='instagram' onClick={this.handleEditProfileBtn}><Icon name='edit'/> Edit Your Profile</Button>
                        </Segment>
                    </Container>
                    <Container style={{marginTop:'1em', marginBottom:'1em'}}>

                        <Segment>
                            <Header>Applied Jobs</Header>
                            <div style={{ minHeight:'300px', maxHeight:"300px", overflowX:"hidden",overflowY:'auto'}}>
                                    {this.state.jobList && this.state.jobList.length!==0?

                                        <Card.Group centered itemsPerRow='3'>
                                            {this.state.jobList.map(item=>{
                                                return  <Jobcard applied={true} data={item} selected={this.selected}/>
                                            })}
                                        </Card.Group>
                                    :
                                    <Message error>
                                        <Message.Header>Not Applied</Message.Header>
                                        <p>
                                        You have not applied for any jobs yet
                                        </p>
                                        <p>
                                        Search for jobs in the jobs page and apply for a job
                                        </p>
                                    </Message>
                                    }
                            </div>
                        </Segment>
                    </Container>

                </Grid>
                <JobDataModal id={this.state.id} open={this.state.openModal} close={this.closeModal} applied={true}/>
                <EditProfileModal open={this.state.openEdit} close={this.handleEditModalClose}/>
                <UploadCVModel open={this.state.open} close={this.closeUpload}/>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch){
    return{
        UserProfileActions: bindActionCreators(UserProfileActions,dispatch),
        CVActions: bindActionCreators(CVActions,dispatch),
        jobActions: bindActionCreators(JobActions,dispatch),
    }
}

function mapStateToProps(state){
    return {
        ...state.UserProfile,
        profileData:state.UserProfile.userProfile,
        // companyData:state.Company.randomCompanies,
    }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(UserProfile))
