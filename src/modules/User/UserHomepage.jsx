import React, { Component } from 'react'
import TopNav from '../../components/TopNav'
import { Grid, Segment, Header, Button, Icon, Dimmer, Loader, CardGroup } from 'semantic-ui-react'
import UserProfileSegment from '../../components/User/UserProfileSegment'
import CompanyCards from '../../components/User/CompanyCards'

import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { UserProfileActions, CompanyActions} from '../../actions'


export class UserHomepage extends Component {

    constructor(props){
        super(props);
        this.state={
            firstName:null,
            lastName:null,
            location:null,
            numberOFJobs:0,
            cvpath:null,
            random:[],
        }
    }

    componentDidMount(){
        this.props.userProfileActions.getUserProfileSegment();
        this.props.companyActions.getRandomCompanies();
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.segmentData!==null && nextProps.companyData!==null){
            console.log("segment", nextProps.segmentData, nextProps.companyData)
            return {
                firstName:nextProps.segmentData.firstName,
                lastName:nextProps.segmentData.lastName,
                location:nextProps.segmentData.location,
                numberOFJobs:nextProps.segmentData.noOfJobsInLocation,
                cvpath:nextProps.segmentData.cvPath,    
                random:nextProps.companyData,
            }
        }
        // if(nextProps.companyData!==null){
        //     console.log("random", nextProps.companyData)
        //     return null
        // }
        else {
            return null
        }
    }

    preSearch=()=>{
        this.props.history.push(`/Jobs?key=${this.state.location}`)
    }

    render() {
        return (
            <div>
                <TopNav/>
                <div style={{ marginTop: '5em', marginLeft:'2em', marginRight:'2em' }}>
                    {this.props.segmentData?
                    <Grid>
                        <Grid.Column width={4}>
                            <Header size='medium'>Welcome back,{this.state.firstName}</Header>
                            <UserProfileSegment 
                            name={`${this.state.firstName} ${this.state.lastName}`} 
                            location={this.state.location}
                            image={this.props.segmentData.image}
                            imagePath={this.props.segmentData.imagePath}
                            cvPath={this.state.cvpath}
                            />
                        </Grid.Column>
                        <Grid.Column width={12}>
                            <Segment>
                                <Header>Jobs in {this.state.location}</Header>
                                <p><Icon name='exclamation circle' color='orange'/>
                                There are currently 
                                <Header size='large' style={{margin:0, marginLeft:'10px', marginRight:'10px', display:'inline-block'}} color="orange">{this.state.numberOFJobs}</Header> 
                                     jobs in {this.state.location}</p>
                                <Button onClick={this.preSearch} fluid>
                                    <Icon name='search'/>Jobs in {this.state.location}
                                </Button>
                            </Segment>
                            <Segment>
                                <Header>Companies you might want to checkout</Header>
                                <p>Check out Companies and Apply for their jobs</p>
                                <br/>
                                <CardGroup itemsPerRow={3}>
                                    {this.state.random!==null? this.state.random.map((item, index)=>{
                                        return(
                                            <CompanyCards
                                            key={index}
                                            id={item.companyId}
                                            name={item.companyName}
                                            rating={item.rating}
                                            description={item.description}
                                            noOfJobs={item.jobs}
                                            location={item.location}
                                            image={item.image}
                                            imagePath={item.imagePath}
                                            props={this.props}
                                            /> 
                                        )
                                    }):null}

                                </CardGroup>
                                <br/>
                                <a href="/AllCompanies">
                                <Header size='small' color='orange'>{'See more Companies >'}</Header></a>
                            </Segment>
                        </Grid.Column>
                    </Grid>
                     :
                     <Dimmer active>
                        <Loader size='medium'>Loading</Loader>
                    </Dimmer>
                    }
                
                </div>
                   
            </div>
        )
    }
}

function mapDispatchToProps(dispatch){
    return{
        userProfileActions: bindActionCreators(UserProfileActions,dispatch),
        companyActions: bindActionCreators(CompanyActions,dispatch),
    }
}

function mapStateToProps(state){
    return {
        ...state.UserProfile,
        segmentData:state.UserProfile.userSegment,
        companyData:state.Company.randomCompanies,
    }
}


export default withRouter(connect(mapStateToProps,mapDispatchToProps) (UserHomepage))
