import React, { Component } from 'react'
import { Header, Image } from 'semantic-ui-react'
import Axios from 'axios'

export class About extends Component {

    constructor(props){
        super(props);
        this.state={
            pic:null,
            array:null
        }
    }

    componentDidMount(){
        Axios.get('/getPic')
        .then(resp=> {
            let file = resp.data
            // var blob = new Blob([resp.data])
            // var file = window.URL.createObjectURL(blob);
            // debugger
            // var link = document.createElement('a');
            // link.href= file

            // link.download = "image.jpeg";
            // link.target ="_blank";
            // link.click();
            // var u8 = new Uint8Array(resp.data);
            // var decoder = new TextDecoder('utf8');
            // var b64encoded = btoa(decoder.decode(u8));
            this.setState({pic:file, array:resp.data})
            // debugger
        }
        )
        .catch(err=>console.log(err))
    }

    render() {
        return (
            <div>
                <Header>About</Header>
                <Image src={`data:image/jpeg;base64,${this.state.pic}`}/>
                <Image src={this.state.pic &&  this.state.pic}/>
                <img id='image'/>
                <p>{this.state.array}</p>
            </div>
        )
    }
}

export default About
