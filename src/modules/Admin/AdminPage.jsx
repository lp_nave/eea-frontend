import React, { Component } from 'react'
import { Header, Segment, Tab, Icon, Button } from 'semantic-ui-react'
import AdminCompanies from '../../components/Admin/AdminCompanies'
import AdminUsers from '../../components/Admin/AdminUsers'
import AdminJobs from '../../components/Admin/AdminJobs'



export class AdminPage extends Component {

    logout=()=>{
        localStorage.removeItem('jwt')
        localStorage.removeItem('email')
        localStorage.removeItem('role')
        this.props.history.push('/Login')
    }

    
    render() {
        const panes = [
            // {
            //   menuItem: 'Companies',
            //   pane:{
            //     key:'comp',
            //     content: (<div><AdminCompanies/></div>)
            //   }
            // },
            { menuItem: 'Companies', render: () => <Tab.Pane><AdminCompanies/></Tab.Pane> },
            { menuItem: 'Users', render: () => <Tab.Pane><AdminUsers/></Tab.Pane> },
            { menuItem: 'Jobs', render: () => <Tab.Pane><AdminJobs/></Tab.Pane> },
          ]
        return (
            <div style={{minHeight:'800px', marginLeft:'2em', marginRight:'2em'}}>
                <Segment>
                    <Header size='huge'>Administrator</Header> 
                    <Button style={{textAlign:'right'}} onClick={this.logout} color='yellow'><Icon name='log out'/> Log out</Button>
                </Segment>
                <Segment>
                        <Tab menu={{secondary:true, pointing:true,color:'orange'}} panes={panes}/>
                </Segment>
                {/* <AdminCompanies/> */}
            </div>
        )
    }
}

export default AdminPage

