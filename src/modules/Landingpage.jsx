import PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Responsive,
  Segment,
  Sidebar,
  Visibility,
  Dropdown,
} from 'semantic-ui-react'

// Heads up!
// We using React Static to prerender our docs with server side rendering, this is a quite simple solution.
// For more advanced usage please check Responsive docs under the "Usage" section.
const getWidth = () => {
  const isSSR = typeof window === 'undefined'

  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth
}

/* eslint-disable react/no-multi-comp */
/* Heads up! HomepageHeading uses inline styling, however it's not the best practice. Use CSS or styled components for
 * such things.
 */
const HomepageHeading = ({ mobile }) => (
  <Container text>
    <Header
      as='h1'
      content='Work Force'
      color='orange'
      inverted
      style={{
        fontSize: mobile ? '2em' : '4em',
        fontWeight: 'normal',
        marginBottom: 0,
        marginTop: mobile ? '1.5em' : '3em',
      }}
    />
    <Header
      as='h2'
      content='Find the job that fits your life'
      inverted
      style={{
        fontSize: mobile ? '1.5em' : '1.7em',
        fontWeight: 'normal',
        marginTop: mobile ? '0.5em' : '1.5em',
      }}
    />
    <Button as='a' href="/login" animated color={"orange"} size='huge'>
      <Button.Content visible>Get Started</Button.Content>
      <Button.Content hidden>
        <Icon name='right arrow' />
      </Button.Content>
    </Button>
  </Container>
)

HomepageHeading.propTypes = {
  mobile: PropTypes.bool,
}

/* Heads up!
 * Neither Semantic UI nor Semantic UI React offer a responsive navbar, however, it can be implemented easily.
 * It can be more complicated, but you can create really flexible markup.
 */
class DesktopContainer extends Component {
  state = {employee:false, employer:false}

  hideFixedMenu = () => this.setState({ fixed: false })
  showFixedMenu = () => this.setState({ fixed: true })

  showEmployees=()=>{
    if(this.state.employee===false){
      this.setState({employee:true, employer:false})
    }else{
      this.setState({employee:false})
    }
  }

  showEmployer=()=>{
    if(this.state.employer===false){
      this.setState({employer:true, employee:false})
    }else{
      this.setState({employer:false})
    }
  }
    

  render() {
    const { children } = this.props
    const { fixed } = this.state
    const {employee} = this.state
    const {employer} = this.state

    return (
      <Responsive getWidth={getWidth} minWidth={Responsive.onlyTablet.minWidth}>
        <Visibility
          once={false}
          onBottomPassed={this.showFixedMenu}
          onBottomPassedReverse={this.hideFixedMenu}
        >
          <Segment
            inverted
            textAlign='center'
            style={{ minHeight: 700, padding: '1em 0em',
            width: "100%",
            height: '100%',
            display: 'inline-block',
            backgroundImage: `url(${"/headerimage.jpg"})`,
            backgroundSize: 'cover',
            marginBottom: 0,
           }}
            vertical
          >
            <Menu
              fixed={fixed ? 'top' : null}
              // inverted={!fixed}
              inverted
              pointing={!fixed}
              secondary={!fixed}
              size='large'
              borderless
              style={{borderWidth:0}}
              
            >
              <Container>
                <Menu.Item>
                  <Image  size='small' src='./homepagelogo.png'/>
                </Menu.Item>
                <Menu.Item as='a' active>
                {/* <Image wrapped size='tiny' src='./homepagelogo.png'/> */}
                  Home
                </Menu.Item>
                <Menu.Item as='a' active={this.state.employee} onClick={this.showEmployees}>
                  For Employees
                </Menu.Item>
                
                <Menu.Item as='a'active={this.state.employer} onClick={this.showEmployer}>
                  For Employers
                </Menu.Item>
                {/* <Menu.Item as='a'>Careers</Menu.Item> */}
                <Menu.Item position='right'>
                  <Button as='a' href="/login" inverted={!fixed}>
                    Log in
                  </Button>
                  <Button as='a' href="/SignUp" inverted={!fixed} primary={fixed} style={{ marginLeft: '0.5em' }}>
                    Sign Up
                  </Button>
                </Menu.Item>
              </Container>
            </Menu>
            {employee===true?
                  <div style={{zIndex:100,top:'50',left:'25%',position:'absolute'}}>
                    <Segment >
                      <Container text>
                        <Header color='teal' style={{marginBottom:'20px'}}>
                          How to get started :Employees
                        </Header>
                        <Container > 
                          <Icon.Group>
                            <Icon color='orange' size='big' name='user circle outline'/>
                            <Header size='tiny'>Create your<br/>profile</Header>
                          </Icon.Group>
                          <Icon.Group style={{marginLeft:'100px'}}>
                            <Icon color='orange' size='big' name='file pdf'/>
                            <Header size='tiny'>Upload your CV <br/>as a pdf</Header>
                          </Icon.Group>
                          <Icon.Group style={{marginLeft:'100px'}}>
                            <Icon color='orange' size='big' name='search'/>
                              <Header size='tiny'>Search for Jobs <br/>or for companies</Header>
                          </Icon.Group>
                          <Icon.Group style={{marginLeft:'100px'}}>
                            <Icon color='orange' size='big' name='briefcase'/>
                            <Header size='tiny'>Apply for <br/>Jobs</Header>
                          </Icon.Group>
                        </Container>
                        </Container>
                    </Segment>
                  </div>
                :null}
            {employer===true?
                  <div style={{zIndex:100,top:'50',left:'25%',position:'absolute'}}>
                    <Segment >
                      <Container text>
                        <Header color='teal' style={{marginBottom:'20px'}}>
                          How to get started :Employeers and Company Account Handlers
                        </Header>
                        <Container > 
                          <Icon.Group>
                            <Icon color='orange' size='big' name='user plus'/>
                            <Header size='tiny'>Create Company<br/>account</Header>
                          </Icon.Group>
                          <Icon.Group style={{marginLeft:'100px'}}>
                            <Icon color='orange' size='big' name='boxes'/>
                            <Header size='tiny'>Add <br/>Departments</Header>
                          </Icon.Group>
                          <Icon.Group style={{marginLeft:'100px'}}>
                            <Icon color='orange' size='big' name='file alternate outline'/>
                              <Header size='tiny'>Publish<br/>Jobs</Header>
                          </Icon.Group>
                          <Icon.Group style={{marginLeft:'100px'}}>
                            <Icon color='orange' size='big' name='download'/>
                            <Header size='tiny'>Download applicant <br/>CV</Header>
                          </Icon.Group>
                        </Container>
                        </Container>
                    </Segment>
                  </div>
                :null}
            <HomepageHeading />
          </Segment>
          
        </Visibility>

        {children}
      </Responsive>
    )
  }
}

DesktopContainer.propTypes = {
  children: PropTypes.node,
}

const ResponsiveContainer = ({ children }) => (
  <div>
    <DesktopContainer>{children}</DesktopContainer>
    {/* <MobileContainer>{children}</MobileContainer> */}
  </div>
)

ResponsiveContainer.propTypes = {
  children: PropTypes.node,
}

const Landingpage = () => (
  <ResponsiveContainer>
    <Segment style={{ padding: '8em 0em' }} vertical>
      <Grid container stackable verticalAlign='middle'>
        <Grid.Row>
          <Grid.Column width={8}>
            <Header as='h3' style={{ fontSize: '2em' }}>
              We Help Employers and Employees
            </Header>
            <p style={{ fontSize: '1.33em' }}>
              We help the unemployed to find jobs and employers to find the perfect person for their
              job by connecting right people for the correct position.
            </p>
            <Header as='h3' style={{ fontSize: '2em' }}>
              All of this is for free!!!
            </Header>
            <p style={{ fontSize: '1.33em' }}>
              Yes that's right!,Our system can be custom tailored for each company's needs on a job hunting website
              and capture the needs of job hunters.
            </p>
          </Grid.Column>
          <Grid.Column floated='right' width={6}>
            <Image bordered rounded size='large' src='./workbuddies.jpg' />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column textAlign='center'>
            <Button color='orange' as='a' href='/SignUp' size='huge'>Check Them Out</Button>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>

    <Segment style={{ padding: '8em 0em' }} vertical>
      <Container text>
        <Header as='h3' style={{ fontSize: '2em', marginBottom:'50px'}}>
          How WORK FORCE Works for Employees
        </Header>
        <Container > 
          <Icon.Group>
            <Icon color='orange' size='huge' name='search'/>
            <Header>Find the<br/> Right JOB</Header>
          </Icon.Group>
          <Icon.Group style={{marginLeft:'100px'}}>
            <Icon color='orange' size='huge' name='searchengin'/>
            <Header>Research <br/>Companies</Header>
          </Icon.Group>
          <Icon.Group style={{marginLeft:'100px'}}>
            <Icon color='orange' size='huge' name='dollar'/><
              Header>Compare <br/>Salaries</Header>
          </Icon.Group>
          <Icon.Group style={{marginLeft:'100px'}}>
            <Icon color='orange' size='huge' name='briefcase'/>
            <Header>Apply for <br/>Jobs</Header>
          </Icon.Group>
        </Container>
       

        <Divider as='h4'className='header' horizontal style={{ margin: '3em 0em'}}>
          <Header color='blue'>AND</Header>
        </Divider>


        <Container text> 
          <Header as='h3' style={{ fontSize: '2em', marginBottom:'50px' }}>
            How WORK FORCE Works for Employers
          </Header>
          <Icon.Group>
            <Icon color='orange' size='huge' name='list'/>
            <Header>list your<br/>Job vacancy</Header>
          </Icon.Group>
          <Icon.Group style={{marginLeft:'100px'}}>
            <Icon color='orange' size='huge' name='clock'/>
            <Header>Wait for <br/>Applications</Header>
          </Icon.Group>
          <Icon.Group style={{marginLeft:'100px'}}>
            <Icon color='orange' size='huge' name='wpforms'/>
              <Header>Compare <br/>CV's</Header>
          </Icon.Group>
          <Icon.Group style={{marginLeft:'100px'}}>
            <Icon color='orange' size='huge' name='check circle outline'/>
            <Header>Hire <br/>Applicant</Header>
          </Icon.Group>
        </Container>
      </Container>
    </Segment>

    <Segment inverted vertical style={{ padding: '5em 0em' }}>
      <Container>
        <Grid divided inverted stackable>
          <Grid.Row>
            <Grid.Column width={3}>
              <Header inverted as='h4' content='About' />
              <List link inverted>
                <List.Item as='a' onClick={()=>{window.open("https://goo.gl/maps/KBTGACQPMURLdfZX7",'_blanck')}} >Maps</List.Item>
                <List.Item as='a'>Contact Us @ <br/>+94 812421348</List.Item>
                {/* <List.Item as='a'></List.Item> */}
                <List.Item as='a'>work4ce@gmail.com</List.Item>
              </List>
            </Grid.Column>
            <Grid.Column width={3}>
              <Header inverted as='h4' content='Services' />
              <List link inverted>
                <List.Item as='a'>Know your worth</List.Item>
                <List.Item as='a'>FAQ</List.Item>
                <List.Item as='a'>How To Upload</List.Item>
              </List>
            </Grid.Column>
            <Grid.Column width={7}>
              <Header as='h3' inverted>
                Find us @
              </Header>
              <Icon style={{marginLeft:'50px'}} size='big' name='facebook'/>
              <Icon style={{marginLeft:'50px'}} size='big' name='twitter'/>
              <Icon style={{marginLeft:'50px'}} size='big' name='google plus'/>
              <Icon style={{marginLeft:'50px'}} size='big' name='linkedin'/>

              
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </Segment>
  </ResponsiveContainer>
)

export default Landingpage