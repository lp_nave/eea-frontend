import React, { Component } from 'react'
import TopNav from '../../components/TopNav'
import CompanyProfileHeader from '../../components/Company/CompanyProfileHeader'
import { Segment, Header, Divider, Grid, GridColumn, Card, Dimmer, Loader, Icon, Message } from 'semantic-ui-react'
import Vacancy from '../../components/Company/Vacancy'
import Jobcard from '../../components/Job/Jobcard'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { CompanyActions, JobActions } from '../../actions'
import JobDataModal from '../../components/Job/JobDataModal'
import { bindActionCreators } from 'redux'

export class Company extends Component {

    constructor(props){
        super(props);
        this.state={
            data:null
        }
    }
    componentDidMount(){
        let idFromLink= this.props.location.search.substring(4)
        this.props.companyActions.viewCompany(idFromLink);
    }

    static getDerivedStateFromProps(nextProps,prevState){
        if(nextProps.data){
            return{
                data:nextProps.data
            }
        }else return null
    }

    selected=(id)=>{
        this.setState({id:id, open:true})
    }

    closeModal=()=>{
        this.setState({open:false, id:null})
        this.props.jobActions.clearJob()
    }

    render() {
        return (
            <div>
                <TopNav/>
                {this.state.data!==null?
                <>
                <CompanyProfileHeader data={this.state.data}/>
                <Segment style={{margin:'2em 4em'}}>
                    <Header color='orange'>Contact Information</Header>
                    <Grid centered columns='3'>
                        <Grid.Column>
                            <Header textAlign='center' as='h4'>
                                <Icon name='phone' color='orange' />
                                <Header.Content >{this.state.data.contactNumber}</Header.Content>
                            </Header>
                        </Grid.Column>
                        <Grid.Column>
                            <Header textAlign='center' as='h4'>
                                <Icon name='at' color='orange'  />
                                <Header.Content>{this.state.data.email}</Header.Content>
                            </Header>
                        </Grid.Column>
                        <Grid.Column>
                            <Header textAlign='center' as='h4'>
                                <Icon name='mail' color='orange' />
                                <Header.Content>{this.state.data.address}</Header.Content>
                            </Header>
                        </Grid.Column>
                    </Grid>
                </Segment>
                <Segment style={{margin:'2em 4em'}}>
                    <Header color='orange'>Vacancies</Header>
                    <br/>
                    <Card.Group centered itemsPerRow='3'>
                    {this.state.data.joblist!==null?
                        <>
                        {this.state.data.joblist.map((item, index)=>{
                            return <Jobcard key={index} data={item} selected={this.selected}/>
                        })}
                        </>
                    :
                    <Message warning compact floating content='No Job listings published yet' />
                    }

                    </Card.Group>
                </Segment>
                <JobDataModal id={this.state.id} open={this.state.open} close={this.closeModal}/>
                </>
            :
            <Dimmer active>
                <Loader>Loading</Loader>
            </Dimmer>
            }
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        // ...state.CompanyProfile,
        // segmentData:state.CompanyProfile.companySegment,
        data:state.Company.viewCompany,

    }
}

function mapDispatchToProps(dispatch){
    return{
        jobActions: bindActionCreators(JobActions,dispatch),
        companyActions: bindActionCreators(CompanyActions,dispatch),
    }
}



export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Company))
