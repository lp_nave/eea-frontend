import React, { Component } from 'react'
import TopNav from '../../components/TopNav'
import { Card, Header, Label, Grid, List, Segment, Dimmer, Loader, Message } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { CVActions, CompanyProfileActions } from '../../actions';
import { bindActionCreators } from 'redux';

export class Applications extends Component {

    constructor(props){
        super(props);
        this.state={

        }
    }

    componentDidMount(){
        this.props.companyProfileActions.getApplicants();
    }

    static getDerivedStateFromProps(nextProps,prevState){
        if(nextProps.applicants){
            return{
                applicants:nextProps.applicants
            }
        }
        else return null
    }

    handleDownload=(email)=>{
        this.props.cvActions.downloadCV(email);
    }

    render() {
        return (
            <div>
                <TopNav/>
                <div style={{height:'560px' ,marginTop: '4em', marginLeft:'2em', marginRight:'2em'}}>
                    {this.state.applicants?
                    <>
                        <Segment>
                            <Header color='orange'>ALL APPLICANTS</Header>
                        </Segment>
                        <Card.Group itemsPerRow={4}>
                        {this.state.applicants && this.state.applicants.map((card,index)=>{
                            debugger
                        return <Card raised key={index}>
                                <Card.Content>
                                    <Card.Header content={card.jobName}/>
                                    <Card.Meta>
                                        <Header style={{marginTop:'5px'}} sub color='teal' size='small'>{card.department}</Header>
                                    </Card.Meta>
                                    <Card.Description>
                                        <div style={{marginTop:'1em',height:'150px' ,overflowY:'auto', overflowX:'hidden'}}>
                                            <List>
                                                {card.applicants.length!=0?  
                                                    <>                                       
                                                        {card.applicants.map((applicant, index)=>{
                                                            return <List.Item as='a' key={index} onClick={()=>{this.handleDownload(applicant.email)}}>
                                                                <Label color='orange'>{`${applicant.firstName} ${applicant.lastName}`}</Label>
                                                            </List.Item>
                                                        })}
                                                    </>
                                                :
                                                <Message warning compact floating content='No applicants yet' />
                                                }
                                            </List>
                                        </div>
                                    </Card.Description>
                                </Card.Content>
                        </Card>
                        })}
                        </Card.Group>
                   
                    </>
                    : 
                    <Dimmer active>
                        <Loader>Loading</Loader>
                    </Dimmer>
                }
                </div>

            </div>
        )
    }
}

function mapDispatchToProps(dispatch){
    return{
        companyProfileActions: bindActionCreators(CompanyProfileActions,dispatch),
        cvActions: bindActionCreators(CVActions,dispatch),
    }
}

function mapStateToProps(state){
    return {
        applicants:state.CompanyProfile.applicants,
        // companyData:state.Company.randomCompanies,
    }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Applications))
