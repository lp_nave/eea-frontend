import React, { Component } from 'react'
import TopNav from '../../components/TopNav'
import { Segment, Card, Dimmer, Loader, Header, Divider } from 'semantic-ui-react'
import CompanyCards from '../../components/User/CompanyCards'
import Searchbar from '../../components/Searchbar'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { CompanyActions } from '../../actions'
import { bindActionCreators } from 'redux'

export class AllCompanies extends Component {

    constructor(props){
        super(props)
        this.state={
            data:null
        }
    }

    componentDidMount(){
        this.props.companyActions.getAllCompanies()
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.allData){
            return{
                data: nextProps.allData
            }
        }
        else return null
    }

    refreshData=()=>{
        this.props.companyActions.getAllCompanies()
    }

    render() {
        return (
            <div>
                <TopNav/>
                <div style={{marginTop:'4em', marginBottom:'2em',marginLeft:'2em', marginRight:'2em'}}>
                    <Searchbar type='company' refresh={this.refreshData}/>
                    <Segment>
                        <Header size='medium' color='orange'>Companies</Header>
                        {/* <Divider horizontal >sklefh</Divider> */}
                        <div style={{margin:'1em',height:'50em', overflow:'auto', overflowX:'hidden'}}>
                        <br/>
                        <Card.Group itemsPerRow={4}>
                        {this.props.allData!==null? this.state.data.map((item, index)=>{
                                    return(
                                        <CompanyCards
                                        key={index}
                                        image={item.image}
                                        imagePath={item.imagePath}
                                        id={item.companyId}
                                        name={item.companyName}
                                        rating={item.rating}
                                        description={item.description}
                                        noOfJobs={item.jobs}
                                        location={item.location}
                                        props={this.props}
                                        /> 
                                    )
                                }):
                                <Dimmer active>
                                    <Loader/>
                                </Dimmer>
                                }
                        </Card.Group>
                        </div>
                    </Segment>
                </div>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch){
    return{
        // companyProfileActions: bindActionCreators(CompanyProfileActions,dispatch),
        companyActions: bindActionCreators(CompanyActions,dispatch),
    }
}

function mapStateToProps(state){
    return {
        allData:state.Company.companies
    }
}


export default withRouter(connect(mapStateToProps,mapDispatchToProps)(AllCompanies))
