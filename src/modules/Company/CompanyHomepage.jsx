import React, { Component } from 'react'
import TopNav from '../../components/TopNav'
import { Grid, Segment, Header, Button, Icon, Dimmer, Loader, CardGroup, Message } from 'semantic-ui-react'
import CompanyProfileSegment from '../../components/Company/CompanyProfileSegment'
import CompanyCards from '../../components/User/CompanyCards'
import Vacancy from '../../components/Company/Vacancy'

import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import {CompanyProfileActions,CompanyActions} from '../../actions'

export class CompanyHomepage extends Component {
    constructor(props){
        super(props);
        this.state={
            companyName:null,
            location:null,
            departments:[],
            jobs:[],
            image:null,
            imagePath:null,
            random:null
        }
    }

    componentDidMount(){
        this.props.companyProfileActions.getCompanySegment();
        this.props.companyActions.getRandomCompanies();
    }

    static getDerivedStateFromProps(nextProps,prevState){
        if(nextProps.segmentData!==null){
            console.log("segment", nextProps.segmentData)
            return{
                companyName:nextProps.segmentData.companyName,
                location: nextProps.segmentData.location,
                departments:nextProps.segmentData.deparments,
                jobs:nextProps.segmentData.jobs,
                image:nextProps.segmentData.image,
                imagePath:nextProps.segmentData.imagePath,
                random:nextProps.companyData,
            }
        }
        else{
            return null
        }
    }

    render() {
        return (
            <div style={{height:'100%'}}>
                <TopNav/>
                <div style={{ marginTop: '5em',  marginLeft:'2em', marginRight:'2em' }}>
                    {this.props.segmentData?                   
                    <Grid>
                        <Grid.Column width={4}>
                            <Header size='large'>Hello , {this.state.companyName}</Header>
                            <CompanyProfileSegment
                            name={this.state.companyName}
                            location={this.state.location}
                            departments={this.state.departments}
                            jobs={this.state.jobs}
                            image={this.state.image}
                            imagePath={this.state.imagePath}
                            />
                        </Grid.Column>
                        <Grid.Column width={12}>
                            {/* <Segment compact={this.state.jobs.length!=0 ? true:false}> */}
                            <Segment>
                                <Header>Your Latest Job vacancies</Header>
    
                                <Vacancy jobs={this.state.jobs}/>
                                {this.state.jobs.length!=0?
                                <>
                                <br/>
                                <Header size='small' as='a' href='/Applicants' color='orange'>
                                    <Header.Content>See More ></Header.Content>    
                                </Header>
                                </>
                                :null }
                            </Segment>
                            <Segment >
                                <Header>Companies you might want to checkout</Header>
                                <p>Follow up on new Companies and stay ahead of the competition</p>
                                <br/>
                                <CardGroup itemsPerRow={3}>
                                    {this.state.random!==null? this.state.random.map((item, index)=>{
                                        return(
                                            <CompanyCards
                                            key={index}
                                            image={item.image}
                                            imagePath={item.imagePath}
                                            id={item.companyId}
                                            name={item.companyName}
                                            rating={item.rating}
                                            description={item.description}
                                            noOfJobs={item.jobs}
                                            location={item.location}
                                            props={this.props}
                                            /> 
                                        )
                                    }):
                                    // <Message icon>
                                    //     <Icon name='circle notched' loading />
                                    //     <Message.Content>
                                    //     <Message.Header>Just one second</Message.Header>
                                    //     We are fetching that content for you.
                                    //     </Message.Content>
                                    // </Message>
                                    <Dimmer active>
                                        <Loader>Loading</Loader>
                                    </Dimmer>
                                    }

                                </CardGroup>
                                <br/>
                                <Header size='small' color='orange' as='a' href='/AllCompanies'>See more Companies ></Header>
                            </Segment>
                        </Grid.Column>
                    </Grid>
                    :
                    <Dimmer active>
                        <Loader size='medium'>Loading</Loader>
                    </Dimmer>
                    }
                </div>
            </div>
        )
    }
}


function mapDispatchToProps(dispatch){
    return{
        companyProfileActions: bindActionCreators(CompanyProfileActions,dispatch),
        companyActions: bindActionCreators(CompanyActions,dispatch),
    }
}

function mapStateToProps(state){
    return {
        ...state.CompanyProfile,
        segmentData:state.CompanyProfile.companySegment,
        companyData:state.Company.randomCompanies,
    }
}



export default withRouter(connect(mapStateToProps,mapDispatchToProps)(CompanyHomepage))
