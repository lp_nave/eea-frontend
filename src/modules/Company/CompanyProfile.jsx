import React, { Component } from 'react'
import CompanyProfileHeader from '../../components/Company/CompanyProfileHeader'
import { Segment, Button, Form, Input, Label, Grid, Container, Header, List, Card, Icon, Message, TransitionablePortal } from 'semantic-ui-react'
import TopNav from '../../components/TopNav'
import Vacancy from '../../components/Company/Vacancy'
import EditCompanyModal from '../../components/Company/EditCompanyModal'
import DepartmentModal from '../../components/Company/DepartmentModal'
import JobVacancyModal from '../../components/Job/JobVacancyModal'
import RemoveDeptConfirmModal from '../../components/Company/RemoveDeptConfirmModal'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { CompanyProfileActions, CompanyActions } from '../../actions'
import { bindActionCreators } from 'redux'

export class Profile extends Component {

    constructor(props){
        super(props);
        this.state={
            departmemt:false,
            profileModal:false,
            vacancyModal:false,
            confirmModal:false,

            profileData:null,
            departmentData:null,
            jobs:null,
            deleteId:null,

            status:null,
            openPortal:false,
        }
    }

    componentDidMount(){
        console.log("props", this.props)
        this.props.companyProfileActions.getCompanyProfile();
        this.props.companyActions.getAllDepartments();
    }

    static getDerivedStateFromProps(nextProps,prevState){
        if(nextProps.profileData && nextProps.departments){
            console.log("data", nextProps.profileData)
            return{
                profileData:nextProps.profileData,
                departmentData: nextProps.departments,
                jobs:nextProps.profileData.jobList
            }
        }
        if(nextProps.deleteStatus!==null){
            debugger
            if(nextProps.deleteStatus===true){
                return{
                    status:'success',
                    openPortal:true
                }
            }
            else if(nextProps.deleteStatus===false){
                return{
                    status:'fail',
                    openPortal:true
                }
            }
        }
        else return null
    }
    
    deparmentBtn=()=>{
        this.setState({departmemt:true})
    }

    handleDepartmentClose=()=>{
        this.setState({departmemt:false})
    }

    handleEditProfileBtn=()=>{
        this.setState({profileModal:true})
    }

    handleEditProfileClose=()=>{
        this.setState({profileModal:false})
    }

    handleVacancyBtn=()=>{
        this.setState({vacancyModal:true})
    }

    handleVacancyModalClose=()=>{
        this.setState({vacancyModal:false})
    }

    handleRemoveDepartment=(e,{id})=>{
        console.log("remove", id)
        this.setState({confirmModal:true, deleteId:id})
    }

    closeRemoveDepartment=()=>{
        this.setState({confirmModal:false})
    }

    saveDepartment=(dept)=>{
        console.log("saa", dept, this.props)
        debugger
        // this.setState({departmemt:false})
        this.props.companyActions.addDepartment(dept)
    }


    render() {
        return (
            <div>
                <TopNav/>
                <Grid textAlign='center'>
                    <CompanyProfileHeader data={this.state.profileData} Ratedisabled/>
                    <Container style={{marginTop:'1em'}}>
                        <Segment>
                            <Button color='black' onClick={this.deparmentBtn}><Icon name='plus square'/>Add Departments</Button>
                            <Button color='orange'onClick={this.handleVacancyBtn}> <Icon name='plus'/>Add Job Vacancies</Button>
                            <Button color='instagram' onClick={this.handleEditProfileBtn}><Icon name='edit'/>Edit Company Profile</Button>
                        </Segment>
                    </Container>
                    <Container style={{marginTop:'1em', marginBottom:'1em'}}>

                        <Segment>
                            <Grid columns={2} divided >
                                <Grid.Column width="8" >
                                    <Header>Departments</Header>
                                    <div style={{margin:'1em',padding:'1em',maxHeight:'400px', overflow:'auto'}}>
                                        {this.state.departmentData?
                                            <List animated>
                                                {this.state.departmentData.map((item,index)=>{
                                                    return <List.Item key={index} id={item.id}><Label size="large" color="orange">
                                                        {item.departmentName}
                                                        <Icon color='black' id={item.id} name='delete' onClick={this.handleRemoveDepartment}/>
                                                    </Label></List.Item>

                                                })}
                                            </List>
                                        :
                                        <Message error>
                                            <Message.Header>No Departments</Message.Header>
                                            <p>
                                            You have not added any departments/teams into your company profile.
                                            Here's an example: "Systems Team", "Finance Department"
                                            </p>
                                        </Message>
                                        }
                                    </div>
                                </Grid.Column>
                                <Grid.Column width='8'>
                                    <Header>Vacancies</Header>
                                    <div style={{margin:'1em',maxHeight:'400px', overflow:'auto', overflowX:'hidden'}}>
                                        <Vacancy jobs={this.state.jobs} company={true}/>
                                    </div>
                                </Grid.Column>
                            </Grid>
                        </Segment>
                    </Container>

                </Grid>

                <DepartmentModal api={this.saveDepartment} openDeparmentModal={this.state.departmemt} closeDepartmentModal={this.handleDepartmentClose}/>
                <EditCompanyModal data={localStorage.getItem("email")} open={this.state.profileModal} close={this.handleEditProfileClose}/>
                <JobVacancyModal edit={false} open={this.state.vacancyModal} close={this.handleVacancyModalClose}/>
                <RemoveDeptConfirmModal deleteId={this.state.deleteId}open={this.state.confirmModal} close={this.closeRemoveDepartment}/>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch){
    return{
        companyProfileActions: bindActionCreators(CompanyProfileActions,dispatch),
        companyActions: bindActionCreators(CompanyActions,dispatch),
    }
}

function mapStateToProps(state){
    return {
        profileData:state.CompanyProfile.companyProfile,
        departments:state.Company.departments,
        deleteStatus:state.Company.deleteStatus,
    }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Profile))
