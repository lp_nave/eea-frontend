import React, { Component } from 'react'
import EmployeeSignUp from '../components/EmployeeSignUp'

import { Form, Input, Header, Segment, Grid, Image, Button, Message } from 'semantic-ui-react'
import CompanySignUp from '../components/CompanySignUp';

export class SignUp extends Component {

    constructor(props){
        super(props);
        this.state={
            companyForm:false
        }
    }

    handleFormToggle=()=>{
        if(this.state.companyForm===true){
            this.setState({companyForm:false})
        }else{
            this.setState({companyForm:true})
        }
    }

    render() {
        return (
            <div style={{
                width: "100%",
                height: '100%',
                display: 'inline-block',
                backgroundImage: `url(${"/signupupdate.jpg"})`,
                backgroundSize: 'cover',
                backgroundPosition:'center',
                marginBottom: 0,
                minHeight:'500px'
            }}>
                <Grid textAlign='center' style={{ height: '130vh' }} verticalAlign='middle'>
                    <Grid.Column style={{ maxWidth: 450 }}>
                        
                    <Image verticalAlign='middle' size='medium' src='/homepagelogo.png' /> 
                    <Header as='h1' inverted textAlign='center'>
                         Sign Up
                    </Header>
                    <Button.Group size='large'>
                        <Button 
                            onClick={this.handleFormToggle}
                            inverted 
                            active={this.state.companyForm===true? false:true} 
                            color={this.state.companyForm==true? null:"orange"} 
                            >Empoyee</Button>
                        <Button.Or />
                        <Button 
                            onClick={this.handleFormToggle}
                            inverted 
                            active={this.state.companyForm===true? true:false}
                            color={this.state.companyForm==true? "orange": null} 
                            >Company</Button>
                    </Button.Group>
                    <div style={{marginTop:'10px'}}>
                        {this.state.companyForm===false?
                            <EmployeeSignUp/>
                        :
                            <CompanySignUp/>
                        }
                    </div>
                    <Message>
                        Already have an account? <a href='/Login'>Login</a>
                    </Message>
                    </Grid.Column>
                </Grid>
                
            </div>
        )
    }
}

export default SignUp
