import {AdminTypes as types} from '../actions';
import { handleActions } from 'redux-actions';

const initialState={
    companies:null,
    jobs:null,
    clients:null
}

export default handleActions({
    [types.ADMIN_GET_COMPANIES]:(state,{payload})=>({
        ...state, laoding:true, companies:null,
    }),
    [types.ADMIN_GET_COMPANIES_SUCCESS]:(state,{payload})=>({
        ...state, laoding:true, companies:payload,
    }),
    [types.ADMIN_GET_COMPANIES_FAIL]:(state,{payload})=>({
        ...state, laoding:true, companies:null,
    }),

    [types.ADMIN_GET_JOBS]:(state,{payload})=>({
        ...state, laoding:true, jobs:null,
    }),
    [types.ADMIN_GET_JOBS_SUCCESS]:(state,{payload})=>({
        ...state, laoding:true, jobs:payload,
    }),
    [types.ADMIN_GET_JOBS_FAIL]:(state,{payload})=>({
        ...state, laoding:true, jobs:null,
    }),
    
    [types.ADMIN_GET_CLIENTS]:(state,{payload})=>({
        ...state, laoding:true, clients:null,
    }),
    [types.ADMIN_GET_CLIENTS_SUCCESS]:(state,{payload})=>({
        ...state, laoding:true, clients:payload,
    }),
    [types.ADMIN_GET_CLIENTS_FAIL]:(state,{payload})=>({
        ...state, laoding:true, clients:null,
    }),
    
    [types.DELETE_COMPANY]:(state,{payload})=>({
        ...state, laoding:true, deleteCompany:null,
    }),
    [types.DELETE_COMPANY_SUCCESS]:(state,{payload})=>({
        ...state, laoding:true, deleteCompany:payload,
    }),
    [types.DELETE_COMPANY_FAIL]:(state,{payload})=>({
        ...state, laoding:true, deleteCompany:null,
    }),
    
    [types.DELETE_CLIENT]:(state,{payload})=>({
        ...state, laoding:true, deleteClient:null,
    }),
    [types.DELETE_CLIENT_SUCCESS]:(state,{payload})=>({
        ...state, laoding:true, deleteClient:payload,
    }),
    [types.DELETE_CLIENT_FAIL]:(state,{payload})=>({
        ...state, laoding:true, deleteClient:null,
    }),
    
    [types.DELETE_JOBS]:(state,{payload})=>({
        ...state, laoding:true, deleteJob:null,
    }),
    [types.DELETE_JOBS_SUCCESS]:(state,{payload})=>({
        ...state, laoding:true, deleteJob:payload,
    }),
    [types.DELETE_JOBS_FAIL]:(state,{payload})=>({
        ...state, laoding:true, deleteJob:null,
    }),



}, initialState)