import {UserProfileTypes as types} from '../actions'

import { handleActions } from 'redux-actions'

const initialState={
    userSegment:null,
    userProfile:null,
    isEdited:null
}

export default handleActions({
    [types.GET_USER_USER_PROFILE_SEGMENT]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.GET_USER_USER_PROFILE_SEGMENT_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,userSegment:payload
    }),
    [types.GET_USER_USER_PROFILE_SEGMENT_FAIL]:(state,{payload})=>({
            ...state,loading:false, error:payload
    }),

    [types.GET_USER_PROFILE]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.GET_USER_PROFILE_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,userProfile:payload
    }),
    [types.GET_USER_PROFILE_FAIL]:(state,{payload})=>({
            ...state,loading:false, error:payload
    }),

    [types.EDIT_USER]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.EDIT_USER_SUCCESS]:(state,{payload})=>({
            ...state,loading:false, isEdited:true
    }),
    [types.EDIT_USER_FAIL]:(state,{payload})=>({
            ...state,loading:false, isEdited:false
    }),

    [types.GET_USER]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.GET_USER_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,user:payload
    }),
    [types.GET_USER_FAIL]:(state,{payload})=>({
            ...state,loading:false, error:payload
    }),

    [types.APPLY_FOR_JOB]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.APPLY_FOR_JOB_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,application:true
    }),
    [types.APPLY_FOR_JOB_FAIL]:(state,{payload})=>({
            ...state,loading:false, application:false
    }),
   

    [types.CLEAR_USER]:(state,{payload})=>({
            ...state,loading:false, isEdited:null, application:null
    }),
}, initialState)