import {authTypes as types }from '../actions';

import {handleActions} from "redux-actions"

const initialState={
    userData:null,
    companyData:null,
    accessLevels:null,
    isAuthenticated:null
}

export default handleActions({
    [types.REGISTER_CLIENT]:(state,{payload})=>({
        ...state,loading:true
    }),
    [types.REGISTER_CLIENT_SUCCESS]:(state,{payload})=>({
        ...state,loading:false,userData:payload, isAuthenticatedClient:true
    }),
    [types.REGISTER_CLIENT_FAIL]:(state,{payload})=>({
        ...state,loading:false,userData:payload, isAuthenticatedClient:false
    }),

    [types.REGISTER_COMPANY]:(state,{payload})=>({
        ...state,loading:true
    }),
    [types.REGISTER_COMPANY_SUCCESS]:(state,{payload})=>({
        ...state,loading:false,companyData:payload, isAuthenticatedCompany:true
    }),
    [types.REGISTER_COMPANY_FAIL]:(state,{payload})=>({
        ...state,loading:false,companyData:payload, isAuthenticatedCompany:false
    }),



    [types.AUTHENTICATE]:(state,{payload})=>({
        ...state,loading:true, isAuthenticated:null
    }),
    [types.AUTHENTICATE_SUCCESS]:(state,{payload})=>({
        ...state,loading:true, isAuthenticated:true
    }),
    [types.AUTHENTICATE_FAIL]:(state,{payload})=>({
        ...state,loading:true, isAuthenticated:false
    }),
    
    [types.CLEAR_AUTH]:(state,{payload})=>({
        ...state,loading:true, isAuthenticated:null
    }),



},initialState)