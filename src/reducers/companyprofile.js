import {CompanyProfileTypes as types} from '../actions'

import { handleActions } from 'redux-actions'

const initialState={
    companySegment:null,
    companyProfile:null,
    isEdit:null,
    company:null,
    applicants:null
}

export default handleActions({
    [types.GET_COMPANY_SEGMENT]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.GET_COMPANY_SEGMENT_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,companySegment:payload
    }),
    [types.GET_COMPANY_SEGMENT_FAIL]:(state,{payload})=>({
            ...state,loading:false, error:payload
    }),

    [types.GET_COMPANY_PROFILE]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.GET_COMPANY_PROFILE_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,companyProfile:payload
    }),
    [types.GET_COMPANY_PROFILE_FAIL]:(state,{payload})=>({
            ...state,loading:false, error:payload
    }),

    [types.EDIT_COMPANY]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.EDIT_COMPANY_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,isEdit:payload
    }),
    [types.EDIT_COMPANY_FAIL]:(state,{payload})=>({
            ...state,loading:false,isEdit:false, error:payload
    }),

    [types.GET_COMPANY]:(state,{payload})=>({
            ...state,loading:true, isEdit:null
    }),
    [types.GET_COMPANY_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,company:payload
    }),
    [types.GET_COMPANY_FAIL]:(state,{payload})=>({
            ...state,loading:false, error:payload
    }),
    
    [types.GET_APPLICANTS]:(state,{payload})=>({
            ...state,loading:true, isEdit:null
    }),
    [types.GET_APPLICANTS_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,applicants:payload
    }),
    [types.GET_APPLICANTS_FAIL]:(state,{payload})=>({
            ...state,loading:false, error:payload
    }),
    
    [types.RATE]:(state,{payload})=>({
            ...state,loading:true, isEdit:null
    }),
    [types.RATE_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,
    }),
    [types.RATE_FAIL]:(state,{payload})=>({
            ...state,loading:false,
    }),

    [types.CLEAR_LOG]:(state,{payload})=>({
            ...state,loading:false, isEdit:null
    }),


}, initialState)