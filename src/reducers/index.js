import {combineReducers} from 'redux'

import authenticate from './authenticate'
import userprofile from './userprofile'
import company from './company';
import companyprofile from './companyprofile';
import jobs from './jobs';
import cv from './cv';
import admin from './admin';


const rootReducer = combineReducers({
    Authenticate:authenticate,
    UserProfile:userprofile,
    Company: company,
    CompanyProfile: companyprofile,
    Jobs:jobs,
    CV:cv,
    Admin:admin
    
})

export default rootReducer;