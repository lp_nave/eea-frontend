import {CVTypes as types} from '../actions'

import { handleActions } from 'redux-actions'

const initialState={
    isUploaded:null,
    image:null

}

export default handleActions({
    [types.UPLOAD_CV]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.UPLOAD_CV_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,isUploaded:true, error:false
    }),
    [types.UPLOAD_CV_FAIL]:(state,{payload})=>({
            ...state,loading:false, isUploaded:false
    }),
    
    [types.DOWNLOAD_CV]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.DOWNLOAD_CV_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,download:payload, error:false
    }),
    [types.DOWNLOAD_CV_FAIL]:(state,{payload})=>({
            ...state,loading:false, error:true
    }),
    
    [types.DOWNLOAD_CV]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.DOWNLOAD_CV_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,image:true, error:false
    }),
    [types.DOWNLOAD_CV_FAIL]:(state,{payload})=>({
            ...state,loading:false, image:false ,error:true
    }),

    [types.CLEAR_CV]:(state,{payload})=>({
            ...state ,isUploaded:null, image:null
    }),

}, initialState)