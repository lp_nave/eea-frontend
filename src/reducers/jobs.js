import {JobTypes as types} from '../actions'

import { handleActions } from 'redux-actions'

const initialState={
    isJobCreated:null,
    jobData:null,
    deleteStatus:null,
    jobs:null
}

export default handleActions({
    [types.CREATE_JOB]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.CREATE_JOB_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,isJobCreated:true
    }),
    [types.CREATE_JOB_FAIL]:(state,{payload})=>({
            ...state,loading:false, isJobCreated:false
    }),

    [types.GET_JOB]:(state,{payload})=>({
            ...state,loading:true,jobData:null
    }),
    [types.GET_JOB_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,jobData:payload
    }),
    [types.GET_JOB_FAIL]:(state,{payload})=>({
            ...state,loading:false, error:true
    }),
    
    [types.GET_ALL_JOBS]:(state,{payload})=>({
            ...state,loading:true,jobs:null
    }),
    [types.GET_ALL_JOBS_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,jobs:payload
    }),
    [types.GET_ALL_JOBS_FAIL]:(state,{payload})=>({
            ...state,loading:false,jobs:null, error:true
    }),

    [types.SEARCH_JOBS]:(state,{payload})=>({
            ...state,loading:true,jobs:null
    }),
    [types.SEARCH_JOBS_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,jobs:payload
    }),
    [types.SEARCH_JOBS_FAIL]:(state,{payload})=>({
            ...state,loading:false,jobs:null, error:true
    }),

    [types.EDIT_JOB]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.EDIT_JOB_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,isEdited:true
    }),
    [types.EDIT_JOB_FAIL]:(state,{payload})=>({
            ...state,loading:false, isEdited:false,error:true
    }),

    [types.DELETE_JOB]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.DELETE_JOB_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,deleteStatus:true
    }),
    [types.DELETE_JOB_FAIL]:(state,{payload})=>({
            ...state,loading:false, deleteStatus:false,error:true
    }),
    


    [types.CLEAR_JOB]:(state,{payload})=>({
            ...state, isEdited:null, deleteStatus:null, jobData:null
    }),
}, initialState)