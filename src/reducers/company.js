import {CompanyTypes as types} from '../actions'

import { handleActions } from 'redux-actions'

const initialState={
    randomCompanies:null,
    isDepartmentAdded:null,
    departments:null,
    deleteStatus:null,
    companies:null
}

export default handleActions({
    [types.GET_RANDOM_COMPANIES]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.GET_RANDOM_COMPANIES_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,randomCompanies:payload, error:false
    }),
    [types.GET_RANDOM_COMPANIES_FAIL]:(state,{payload})=>({
            ...state,loading:false, error:payload
    }),

    [types.ADD_DEPARTMENT]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.ADD_DEPARTMENT_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,isDepartmentAdded:true, error:false
    }),
    [types.ADD_DEPARTMENT_FAIL]:(state,{payload})=>({
            ...state,loading:false,isDepartmentAdded:false, error:payload
    }),
   
    [types.GET_ALL_DEPARTMENTS]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.GET_ALL_DEPARTMENTS_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,departments:payload, error:false
    }),
    [types.GET_ALL_DEPARTMENTS_FAIL]:(state,{payload})=>({
            ...state,loading:false, error:payload
    }),
    
    [types.DELETE_DEPARTMENT]:(state,{payload})=>({
            ...state,loading:true
    }),
    [types.DELETE_DEPARTMENT_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,deleteStatus:true, error:false
    }),
    [types.DELETE_DEPARTMENT_FAIL]:(state,{payload})=>({
            ...state,loading:false, deleteStatus:false,error:true
    }),

    [types.GET_ALL_COMPANIES]:(state,{payload})=>({
            ...state,loading:true,companies:null
    }),
    [types.GET_ALL_COMPANIES_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,companies:payload, error:false
    }),
    [types.GET_ALL_COMPANIES_FAIL]:(state,{payload})=>({
            ...state,loading:false, companies:[],error:payload
    }),
    
    [types.SEARCH_COMPANIES]:(state,{payload})=>({
            ...state,loading:true,companies:null
    }),
    [types.SEARCH_COMPANIES_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,companies:payload, error:false
    }),
    [types.SEARCH_COMPANIES_FAIL]:(state,{payload})=>({
            ...state,error:true
    }),
    
    [types.VIEW_COMPANY]:(state,{payload})=>({
            ...state,loading:true,viewCompany:null
    }),
    [types.VIEW_COMPANY_SUCCESS]:(state,{payload})=>({
            ...state,loading:false,viewCompany:payload, error:false
    }),
    [types.VIEW_COMPANY_FAIL]:(state,{payload})=>({
            ...state,loading:false,error:true
    }),

    [types.CLEAR_DEPARTMENT_STATUS]:(state,{payload})=>({
            ...state, deleteStatus:null,error:null, isDepartmentAdded:null
    }),


}, initialState)