import {createAction} from 'redux-actions'

export const GET_RANDOM_COMPANIES="GET_RANDOM_COMPANIES";
export const GET_RANDOM_COMPANIES_SUCCESS="GET_RANDOM_COMPANIES_SUCCESS";
export const GET_RANDOM_COMPANIES_FAIL="GET_RANDOM_COMPANIES_FAIL";

export const ADD_DEPARTMENT="ADD_DEPARTMENT";
export const ADD_DEPARTMENT_SUCCESS="ADD_DEPARTMENT_SUCCESS";
export const ADD_DEPARTMENT_FAIL="ADD_DEPARTMENT_FAIL";

export const GET_ALL_DEPARTMENTS="GET_ALL_DEPARTMENTS";
export const GET_ALL_DEPARTMENTS_SUCCESS="GET_ALL_DEPARTMENTS_SUCCESS";
export const GET_ALL_DEPARTMENTS_FAIL="GET_ALL_DEPARTMENTS_FAIL";

export const DELETE_DEPARTMENT="DELETE_DEPARTMENT";
export const DELETE_DEPARTMENT_SUCCESS="DELETE_DEPARTMENT_SUCCESS";
export const DELETE_DEPARTMENT_FAIL="DELETE_DEPARTMENT_FAIL";

export const GET_ALL_COMPANIES="GET_ALL_COMPANIES";
export const GET_ALL_COMPANIES_SUCCESS="GET_ALL_COMPANIES_SUCCESS";
export const GET_ALL_COMPANIES_FAIL="GET_ALL_COMPANIES_FAIL";

export const SEARCH_COMPANIES="SEARCH_COMPANIES";
export const SEARCH_COMPANIES_SUCCESS="SEARCH_COMPANIES_SUCCESS";
export const SEARCH_COMPANIES_FAIL="SEARCH_COMPANIES_FAIL";

export const VIEW_COMPANY="VIEW_COMPANY";
export const VIEW_COMPANY_SUCCESS="VIEW_COMPANY_SUCCESS";
export const VIEW_COMPANY_FAIL="VIEW_COMPANY_FAIL";

export const CLEAR_DEPARTMENT_STATUS= "CLEAR_DEPARTMENT_STATUS";



export default {
    getRandomCompanies : createAction(GET_RANDOM_COMPANIES),
    getRandomCompaniesSuccess : createAction(GET_RANDOM_COMPANIES_SUCCESS),
    getRandomCompaniesFail : createAction(GET_RANDOM_COMPANIES_FAIL),
    
    addDepartment : createAction(ADD_DEPARTMENT),
    addDepartmentSuccess : createAction(ADD_DEPARTMENT_SUCCESS),
    addDepartmentFail : createAction(ADD_DEPARTMENT_FAIL),
    
    getAllDepartments : createAction(GET_ALL_DEPARTMENTS),
    getAllDepartmentsSuccess : createAction(GET_ALL_DEPARTMENTS_SUCCESS),
    getAllDepartmentsFail : createAction(GET_ALL_DEPARTMENTS_FAIL),

    deleteDepartment : createAction(DELETE_DEPARTMENT),
    deleteDepartmentSuccess : createAction(DELETE_DEPARTMENT_SUCCESS),
    deleteDepartmentFail : createAction(DELETE_DEPARTMENT_FAIL),

    getAllCompanies: createAction(GET_ALL_COMPANIES),
    getAllCompaniesSuccess: createAction(GET_ALL_COMPANIES_SUCCESS),
    getAllCompaniesFail: createAction(GET_ALL_COMPANIES_FAIL),
    
    searchCompanies: createAction(SEARCH_COMPANIES),
    searchCompaniesSuccess: createAction(SEARCH_COMPANIES_SUCCESS),
    searchCompaniesFail: createAction(SEARCH_COMPANIES_FAIL),
    
    viewCompany: createAction(VIEW_COMPANY),
    viewCompanySuccess: createAction(VIEW_COMPANY_SUCCESS),
    viewCompanyFail: createAction(VIEW_COMPANY_FAIL),

    clearDepartmentStatus : createAction(CLEAR_DEPARTMENT_STATUS),

}