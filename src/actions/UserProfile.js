import {createAction} from 'redux-actions'

export const GET_USER_USER_PROFILE_SEGMENT="GET_USER_USER_PROFILE_SEGMENT";
export const GET_USER_USER_PROFILE_SEGMENT_SUCCESS="GET_USER_USER_PROFILE_SEGMENT_SUCCESS";
export const GET_USER_USER_PROFILE_SEGMENT_FAIL="GET_USER_USER_PROFILE_SEGMENT_FAIL";

export const GET_USER_PROFILE="GET_USER_PROFILE";
export const GET_USER_PROFILE_SUCCESS="GET_USER_PROFILE_SUCCESS";
export const GET_USER_PROFILE_FAIL="GET_USER_PROFILE_FAIL";

export const EDIT_USER="EDIT_USER";
export const EDIT_USER_SUCCESS="EDIT_USER_SUCCESS";
export const EDIT_USER_FAIL="EDIT_USER_FAIL";

export const GET_USER="GET_USER";
export const GET_USER_SUCCESS="GET_USER_SUCCESS";
export const GET_USER_FAIL="GET_USER_FAIL";

export const APPLY_FOR_JOB="APPLY_FOR_JOB";
export const APPLY_FOR_JOB_SUCCESS="APPLY_FOR_JOB_SUCCESS";
export const APPLY_FOR_JOB_FAIL="APPLY_FOR_JOB_FAIL";

export const CLEAR_USER="CLEAR_USER";


export default {
    getUserProfileSegment : createAction(GET_USER_USER_PROFILE_SEGMENT),
    getUserProfileSegmentSuccess : createAction(GET_USER_USER_PROFILE_SEGMENT_SUCCESS),
    getUserProfileSegmentFail : createAction(GET_USER_USER_PROFILE_SEGMENT_FAIL),

    getUserProfile : createAction(GET_USER_PROFILE),
    getUserProfileSuccess : createAction(GET_USER_PROFILE_SUCCESS),
    getUserProfileFail : createAction(GET_USER_PROFILE_FAIL),
  
    editUser : createAction(EDIT_USER),
    editUserSuccess : createAction(EDIT_USER_SUCCESS),
    editUserFail : createAction(EDIT_USER_FAIL),

    getUser : createAction(GET_USER),
    getUserSuccess : createAction(GET_USER_SUCCESS),
    getUserFail : createAction(GET_USER_FAIL),

    applyForJob : createAction(APPLY_FOR_JOB),
    applyForJobSuccess : createAction(APPLY_FOR_JOB_SUCCESS),
    applyForJobFail : createAction(APPLY_FOR_JOB_FAIL),

    clearUser : createAction(CLEAR_USER)

}