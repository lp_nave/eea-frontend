import { createAction } from "redux-actions";

export const ADMIN_GET_COMPANIES = "ADMIN_GET_COMPANIES";
export const ADMIN_GET_COMPANIES_SUCCESS = "ADMIN_GET_COMPANIES_SUCCESS";
export const ADMIN_GET_COMPANIES_FAIL = "ADMIN_GET_COMPANIES_FAIL";

export const ADMIN_DOWNLOAD_COMPANIES = "ADMIN_DOWNLOAD_COMPANIES";
export const ADMIN_DOWNLOAD_COMPANIES_SUCCESS = "ADMIN_DOWNLOAD_COMPANIES_SUCCESS";
export const ADMIN_DOWNLOAD_COMPANIES_FAIL = "ADMIN_DOWNLOAD_COMPANIES_FAIL";

export const ADMIN_GET_JOBS = "ADMIN_GET_JOBS";
export const ADMIN_GET_JOBS_SUCCESS = "ADMIN_GET_JOBS_SUCCESS";
export const ADMIN_GET_JOBS_FAIL = "ADMIN_GET_JOBS_FAIL";

export const ADMIN_DOWNLOAD_JOBS = "ADMIN_DOWNLOAD_JOBS";
export const ADMIN_DOWNLOAD_JOBS_SUCCESS = "ADMIN_DOWNLOAD_JOBS_SUCCESS";
export const ADMIN_DOWNLOAD_JOBS_FAIL = "ADMIN_DOWNLOAD_JOBS_FAIL";

export const ADMIN_GET_CLIENTS = "ADMIN_GET_CLIENTS";
export const ADMIN_GET_CLIENTS_SUCCESS = "ADMIN_GET_CLIENTS_SUCCESS";
export const ADMIN_GET_CLIENTS_FAIL = "ADMIN_GET_CLIENTS_FAIL";

export const ADMIN_DOWNLOAD_CLIENTS = "ADMIN_DOWNLOAD_CLIENTS";
export const ADMIN_DOWNLOAD_CLIENTS_SUCCESS = "ADMIN_DOWNLOAD_CLIENTS_SUCCESS";
export const ADMIN_DOWNLOAD_CLIENTS_FAIL = "ADMIN_DOWNLOAD_CLIENTS_FAIL"; 

export const DELETE_COMPANY = "DELETE_COMPANY";
export const DELETE_COMPANY_SUCCESS = "DELETE_COMPANY_SUCCESS";
export const DELETE_COMPANY_FAIL = "DELETE_COMPANY_FAIL"; 

export const DELETE_CLIENT = "DELETE_CLIENT";
export const DELETE_CLIENT_SUCCESS = "DELETE_CLIENT_SUCCESS";
export const DELETE_CLIENT_FAIL = "DELETE_CLIENT_FAIL"; 

export const DELETE_JOBS = "DELETE_JOBS";
export const DELETE_JOBS_SUCCESS = "DELETE_JOBS_SUCCESS";
export const DELETE_JOBS_FAIL = "DELETE_JOBS_FAIL"; 

export default {
    adminCompanies:createAction(ADMIN_GET_COMPANIES),
    adminCompaniesSuccess:createAction(ADMIN_GET_COMPANIES_SUCCESS),
    adminCompaniesFail:createAction(ADMIN_GET_COMPANIES_FAIL),
    
    adminDownloadCompanies:createAction(ADMIN_DOWNLOAD_COMPANIES),
    adminDownloadCompaniesSuccess:createAction(ADMIN_DOWNLOAD_COMPANIES_SUCCESS),
    adminDownloadCompaniesFail:createAction(ADMIN_DOWNLOAD_COMPANIES_FAIL),

    adminJobs:createAction(ADMIN_GET_JOBS),
    adminJobsSuccess:createAction(ADMIN_GET_JOBS_SUCCESS),
    adminJobsFail:createAction(ADMIN_GET_JOBS_FAIL),
    
    adminDownloadJobs:createAction(ADMIN_DOWNLOAD_JOBS),
    adminDownloadJobsSuccess:createAction(ADMIN_DOWNLOAD_JOBS_SUCCESS),
    adminDownloadJobsFail:createAction(ADMIN_DOWNLOAD_JOBS_FAIL),
    
    adminClients:createAction(ADMIN_GET_CLIENTS),
    adminClientsSuccess:createAction(ADMIN_GET_CLIENTS_SUCCESS),
    adminClientsFail:createAction(ADMIN_GET_CLIENTS_FAIL),
    
    adminDownloadClients:createAction(ADMIN_DOWNLOAD_CLIENTS),
    adminDownloadClientsSuccess:createAction(ADMIN_DOWNLOAD_CLIENTS_SUCCESS),
    adminDownloadClientsFail:createAction(ADMIN_DOWNLOAD_CLIENTS_FAIL),
    
    deleteCompany:createAction(DELETE_COMPANY),
    deleteCompanySuccess:createAction(DELETE_COMPANY_SUCCESS),
    deleteCompanyFail:createAction(DELETE_COMPANY_FAIL),
    
    deleteClient:createAction(DELETE_CLIENT),
    deleteClientSuccess:createAction(DELETE_CLIENT_SUCCESS),
    deleteClientFail:createAction(DELETE_CLIENT_FAIL),
    
    deleteJobs:createAction(DELETE_JOBS),
    deleteJobsSuccess:createAction(DELETE_JOBS_SUCCESS),
    deleteJobsFail:createAction(DELETE_JOBS_FAIL),

}

