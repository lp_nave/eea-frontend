import {createAction} from 'redux-actions'

export const GET_COMPANY_SEGMENT="GET_COMPANY_SEGMENT";
export const GET_COMPANY_SEGMENT_SUCCESS="GET_COMPANY_SEGMENT_SUCCESS";
export const GET_COMPANY_SEGMENT_FAIL="GET_COMPANY_SEGMENT_FAIL";

export const GET_COMPANY_PROFILE="GET_COMPANY_PROFILE";
export const GET_COMPANY_PROFILE_SUCCESS="GET_COMPANY_PROFILE_SUCCESS";
export const GET_COMPANY_PROFILE_FAIL="GET_COMPANY_PROFILE_FAIL";

export const EDIT_COMPANY="EDIT_COMPANY";
export const EDIT_COMPANY_SUCCESS="EDIT_COMPANY_SUCCESS";
export const EDIT_COMPANY_FAIL="EDIT_COMPANY_FAIL";

export const CLEAR_LOG="CLEAR_LOG";

export const GET_COMPANY="GET_COMPANY";
export const GET_COMPANY_SUCCESS="GET_COMPANY_SUCCESS";
export const GET_COMPANY_FAIL="GET_COMPANY_FAIL";

export const GET_APPLICANTS="GET_APPLICANTS";
export const GET_APPLICANTS_SUCCESS="GET_APPLICANTS_SUCCESS";
export const GET_APPLICANTS_FAIL="GET_APPLICANTS_FAIL";

export const RATE="RATE";
export const RATE_SUCCESS="RATE_SUCCESS";
export const RATE_FAIL="RATE_FAIL";



export default {
    getCompanySegment : createAction(GET_COMPANY_SEGMENT),
    getCompanySegmentSuccess : createAction(GET_COMPANY_SEGMENT_SUCCESS),
    getCompanySegmentFail : createAction(GET_COMPANY_SEGMENT_FAIL),

    getCompanyProfile : createAction(GET_COMPANY_PROFILE),
    getCompanyProfileSuccess : createAction(GET_COMPANY_PROFILE_SUCCESS),
    getCompanyProfileFail : createAction(GET_COMPANY_PROFILE_FAIL),

    editCompany : createAction(EDIT_COMPANY),
    editCompanySuccess : createAction(EDIT_COMPANY_SUCCESS),
    editCompanyFail : createAction(EDIT_COMPANY_FAIL),

    clearStatusLog:createAction(CLEAR_LOG),
    
    getCompany : createAction(GET_COMPANY),
    getCompanySuccess : createAction(GET_COMPANY_SUCCESS),
    getCompanyFail : createAction(GET_COMPANY_FAIL),
    
    getApplicants : createAction(GET_APPLICANTS),
    getApplicantsSuccess : createAction(GET_APPLICANTS_SUCCESS),
    getApplicantsFail : createAction(GET_APPLICANTS_FAIL),
    
    rate : createAction(RATE),
    rateSuccess : createAction(RATE_SUCCESS),
    rateFail : createAction(RATE_FAIL),

}