import { createAction } from "redux-actions";

export const UPLOAD_CV = "UPLOAD_CV";
export const UPLOAD_CV_SUCCESS = "UPLOAD_CV_SUCCESS";
export const UPLOAD_CV_FAIL = "UPLOAD_CV_FAIL";

export const DOWNLOAD_CV = "DOWNLOAD_CV";
export const DOWNLOAD_CV_SUCCESS = "DOWNLOAD_CV_SUCCESS";
export const DOWNLOAD_CV_FAIL = "DOWNLOAD_CV_FAIL";

export const UPLOAD_IMAGE = "UPLOAD_IMAGE";
export const UPLOAD_IMAGE_SUCCESS = "UPLOAD_IMAGE_SUCCESS";
export const UPLOAD_IMAGE_FAIL = "UPLOAD_IMAGE_FAIL";

export const CLEAR_CV="CLEAR_CV";

export default {
    uploadCV : createAction(UPLOAD_CV),
    uploadCVSuccess : createAction(UPLOAD_CV_SUCCESS),
    uploadCVFail : createAction(UPLOAD_CV_FAIL),
    
    downloadCV : createAction(DOWNLOAD_CV),
    downloadCVSuccess : createAction(DOWNLOAD_CV_SUCCESS),
    downloadCVFail : createAction(DOWNLOAD_CV_FAIL),
    
    uploadImage : createAction(UPLOAD_IMAGE),
    uploadImageSuccess : createAction(UPLOAD_IMAGE_SUCCESS),
    uploadImageFail : createAction(UPLOAD_IMAGE_FAIL),

    clearCV: createAction(CLEAR_CV)
}