import * as Auth from './Authenticate';
import * as UserProfile from './UserProfile';
import * as Company from './Company';
import * as CompanyProfile from './CompanyProfile';
import * as Jobs from './Jobs';
import * as CV from './CV';
import * as Admin from './Admin';

export {
    Auth as authTypes
}
export const authActions = Auth.default;

export {
    UserProfile as UserProfileTypes
}
export const UserProfileActions = UserProfile.default;

export {
    Company as CompanyTypes
}
export const CompanyActions = Company.default;

export {
    CompanyProfile as CompanyProfileTypes
}
export const CompanyProfileActions = CompanyProfile.default;

export {
    Jobs as JobTypes
}
export const JobActions = Jobs.default;

export {
    CV as CVTypes
}
export const CVActions = CV.default;

export {
    Admin as AdminTypes
}
export const AdminActions = Admin.default;
