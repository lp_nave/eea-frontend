import {createAction} from 'redux-actions'

const action_header= "user/"

export const REGISTER_CLIENT = action_header+"REGISTER_CLIENT";
export const REGISTER_CLIENT_SUCCESS = action_header+"REGISTER_CLIENT_SUCCESS";
export const REGISTER_CLIENT_FAIL = action_header+"REGISTER_CLIENT_FAIL";

export const REGISTER_COMPANY = action_header+"REGISTER_COMPANY";
export const REGISTER_COMPANY_SUCCESS = action_header+"REGISTER_COMPANY_SUCCESS";
export const REGISTER_COMPANY_FAIL = action_header+"REGISTER_COMPANY_FAIL";

export const AUTHENTICATE = action_header+"AUTHENTICATE";
export const AUTHENTICATE_SUCCESS = action_header+"AUTHENTICATE_SUCCESS";
export const AUTHENTICATE_FAIL = action_header+"AUTHENTICATE_FAIL";

export const CLEAR_AUTH = action_header+"CLEAR_AUTH";


export default {
    registerClient: createAction(REGISTER_CLIENT),
    registerClientSuccess: createAction(REGISTER_CLIENT_SUCCESS),
    registerClientFail: createAction(REGISTER_CLIENT_FAIL),

    registerCompany: createAction(REGISTER_COMPANY),
    registerCompanySuccess: createAction(REGISTER_COMPANY_SUCCESS),
    registerCompanyFail: createAction(REGISTER_COMPANY_FAIL),   

    authenticate: createAction(AUTHENTICATE),
    authenticateSuccess: createAction(AUTHENTICATE_SUCCESS),
    authenticateFail: createAction(AUTHENTICATE_FAIL),

    clearAuth:createAction(CLEAR_AUTH),
}