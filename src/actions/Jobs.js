import {createAction} from 'redux-actions'

export const CREATE_JOB="CREATE_JOB";
export const CREATE_JOB_SUCCESS="CREATE_JOB_SUCCESS";
export const CREATE_JOB_FAIL="CREATE_JOB_FAIL";

export const GET_JOB="GET_JOB";
export const GET_JOB_SUCCESS="GET_JOB_SUCCESS";
export const GET_JOB_FAIL="GET_JOB_FAIL";

export const GET_ALL_JOBS="GET_ALL_JOBS";
export const GET_ALL_JOBS_SUCCESS="GET_ALL_JOBS_SUCCESS";
export const GET_ALL_JOBS_FAIL="GET_ALL_JOBS_FAIL";

export const EDIT_JOB="EDIT_JOB";
export const EDIT_JOB_SUCCESS="EDIT_JOB_SUCCESS";
export const EDIT_JOB_FAIL="EDIT_JOB_FAIL";

export const DELETE_JOB="DELETE_JOB";
export const DELETE_JOB_SUCCESS="DELETE_JOB_SUCCESS";
export const DELETE_JOB_FAIL="DELETE_JOB_FAIL";

export const SEARCH_JOBS="SEARCH_JOBS";
export const SEARCH_JOBS_SUCCESS="SEARCH_JOBS_SUCCESS";
export const SEARCH_JOBS_FAIL="SEARCH_JOBS_FAIL";

export const VIEW_JOB="VIEW_JOB";
export const VIEW_JOB_SUCCESS="VIEW_JOB_SUCCESS";
export const VIEW_JOB_FAIL="VIEW_JOB_FAIL";

export const CLEAR_JOB ="CLEAR_JOB"

export default {
    createJob : createAction(CREATE_JOB),
    createJobSuccess : createAction(CREATE_JOB_SUCCESS),
    createJobFail : createAction(CREATE_JOB_FAIL),
    
    getJob : createAction(GET_JOB),
    getJobSuccess : createAction(GET_JOB_SUCCESS),
    getJobFail : createAction(GET_JOB_FAIL),
    
    getAllJobs : createAction(GET_ALL_JOBS),
    getAllJobsSuccess : createAction(GET_ALL_JOBS_SUCCESS),
    getAllJobsFail : createAction(GET_ALL_JOBS_FAIL),
    
    editJob : createAction(EDIT_JOB),
    editJobSuccess : createAction(EDIT_JOB_SUCCESS),
    editJobFail : createAction(EDIT_JOB_FAIL),

    deleteJob : createAction(DELETE_JOB),
    deleteJobSuccess : createAction(DELETE_JOB_SUCCESS),
    deleteJobFail : createAction(DELETE_JOB_FAIL),
    
    searchJobs : createAction(SEARCH_JOBS),
    searchJobsSuccess : createAction(SEARCH_JOBS_SUCCESS),
    searchJobsFail : createAction(SEARCH_JOBS_FAIL),

    clearJob : createAction(CLEAR_JOB),

}