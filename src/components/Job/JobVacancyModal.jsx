import React, { Component } from 'react'
import { Modal, Form, Button, TextArea, Dropdown, Dimmer, Loader, TransitionablePortal, Segment, Header } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { CompanyActions,JobActions } from '../../actions';
import { bindActionCreators } from 'redux';

export class JobVacancyModal extends Component {

    constructor(props){
        super(props);
        this.state={
            departmentOptions:[],
            id:null,
            jobTitle:'',
            description:'',
            requirements:'',
            type:'',
            salary:'',
            department:null,
            location:'',

            deptData:null,

            jobData:null,
            apiCalled:null,

            status:null,
            openPortal:false
        }   
        this.handleInputs = this.handleInputs.bind(this); 
    }

    componentDidMount(){
        console.log("props", this.props)
        // if(this.props.edit===true){
            // this.props.jobActions.getJob(this.props.id)
        if(this.props.edit===false){
            this.props.companyActions.getAllDepartments();
        }
    }

    static getDerivedStateFromProps(nextProps,prevState){
        // debugger
        if(JSON.stringify(nextProps.departmentData) !== JSON.stringify(prevState.deptData)){
            debugger
            console.log("department data", nextProps.departmentData)
            let result =[]
            result = nextProps.departmentData;
            let deptList=[]
            result.map(item=>{
                deptList.push({key:item.id, value:item.id, text:item.departmentName})
            })
            return{
                deptData:nextProps.departmentData,
                departmentOptions: deptList
            }
        }
        if(nextProps.edit===true && nextProps.id !==null && nextProps.id!==prevState.id){
            debugger
            nextProps.jobActions.getJob(nextProps.id)
            return{
                id:nextProps.id,
                apiCalled:true
            }
        }
        if(nextProps.jobData!==null && JSON.stringify(nextProps.jobData) !== JSON.stringify(prevState.jobData) && nextProps.edit===true){
            debugger
            console.log("job",nextProps.jobData)
            let deptname;
            // nextProps.departmentData.map(item=>{
            //     debugger
            //     if(item.id===nextProps.jobData.deptId){
            //         deptname =item.departmentName
            //     }
            // })

            
            return{
                jobData:nextProps.jobData,
                id:nextProps.jobData.jobId,
                jobTitle:nextProps.jobData.name,
                description:nextProps.jobData.description,
                requirements:nextProps.jobData.requirements,
                type:nextProps.jobData.type,
                salary:nextProps.jobData.salary,
                department:nextProps.jobData.deptId,
                location:nextProps.jobData.location,
                apiCalled:false
            }
        }
        if(nextProps.isEdit!==null){
            if(nextProps.isEdit===true){
                return{
                    status:"success",
                    openPortal:true
                }
            }else if(nextProps.isEdit===false){
                return{
                    status:"fail",
                    openPortal:true
                }
            }else return null
        }
        else{
            return null
        }
    }

    async handleInputs(e,{value, name}){
        await this.setState({
            // jobTitle:value
            [name]:value
        })
        console.log(this.state)
    }

    handleDropDown=(e,{value})=>{
       this.setState({department:value})
    }

    createJob=(e)=>{
        debugger
        this.props.jobActions.createJob(this.state)
        this.props.close()
    }

    editjob=()=>{
        debugger
        this.props.close()
        this.props.jobActions.editJob(this.state)
    }

    handleClose=()=>{
        debugger
        this.props.jobActions.clearJob()
        this.setState({openPortal:false, status:null})
        this.props.close()
    }

    render() {
        return (
            <div>
                <Modal open={this.props.open} onClose={this.props.close} >
                <Modal.Header>Add Job Vacancy</Modal.Header>
                <Modal.Content scrolling>
                    {this.props.edit===true && this.state.apiCalled==true?
                        <Dimmer active>
                            <Loader />
                        </Dimmer>
                    :null}
                    <Modal.Description>
                        <Form onSubmit={this.props.edit===true?this.editjob :this.createJob}>
                            <Form.Input value={this.state.jobTitle} required name="jobTitle" onChange={this.handleInputs} focus fluid placeholder='Job Titile' />   
                            <Form.TextArea value={this.state.description} required name="description" onChange={this.handleInputs} placeholder='Description' style={{ minHeight: 100 }} />
                            <Form.TextArea value={this.state.requirements} required name="requirements" onChange={this.handleInputs} placeholder='Requirement' style={{ minHeight: 100 }} />
                            <Form.Input value={this.state.type} required name='type' focus fluid onChange={this.handleInputs} placeholder='Type (eg: Internship , Permanent Staff)' />
                            <Form.Input value={this.state.salary} type='number' required name='salary' focus fluid onChange={this.handleInputs} placeholder='Sallary' />
                            {this.props.edit===false?
                                <Form.Dropdown
                                    placeholder='Search for Department'
                                    fluid
                                    search
                                    selection
                                    button
                                    // defaultValue={this.state.departmentOptions[0]}
                                    onChange={this.handleDropDown}
                                    options={this.state.departmentOptions}
                                />
                            :
                            <Header size='small'>Department: {this.state.department}</Header>
                            }
                            <Form.Input value={this.state.location} required name='location' focus fluid onChange={this.handleInputs} placeholder='Location of job' />
                            {this.props.edit===true?
                                <Button color="orange" type='submit'>
                                    Edit Vacancy
                                </Button>
                            :
                                <Button color="orange" type='submit'>
                                    Publish Vacancy
                                </Button>
                            }
                        </Form>
                    </Modal.Description>
                </Modal.Content>
                
                <TransitionablePortal open={this.state.openPortal} onClose={this.handleClose} transition={{animation:"fly up", duration:500}}>
                        <Segment color='orange' style={{  backgroundColor:'lightgrey',left: '40%', position: 'fixed', botom: '5%', zIndex: 1000 }}>
                        {this.state.status==="success"?
                            <>
                                <Header color='green'>Success!</Header>
                                <p>The job listing has been sucesfully removed</p>
                            </>
                        :null}
                        {this.state.status==='fail'?
                            <>
                                <Header color='red'>Failed!</Header>
                                <p>Failed to remove the job listing</p>
                                <p>Please try again</p>
                            </>
                        :null}
                        <Button fluid onClick={this.handleClose} content="Close" negative/>
                        </Segment>
                </TransitionablePortal>
            </Modal>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch){
    return{
        jobActions: bindActionCreators(JobActions,dispatch),
        companyActions: bindActionCreators(CompanyActions,dispatch),
    }
}

function mapStateToProps(state){
    return {
        // ...state.CompanyProfile,
        // segmentData:state.CompanyProfile.companySegment,
        departmentData:state.Company.departments,
        jobData:state.Jobs.jobData,
        isEdit:state.Jobs.isEdited,
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(JobVacancyModal)
