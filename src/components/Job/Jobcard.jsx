import React, { Component } from 'react'
import { Card, Header } from 'semantic-ui-react'
import JobDataModal from './JobDataModal'

export class Jobcard extends Component {
    constructor(props){
        super(props);
        this.state={
            open:false,
            id:null
        }
    }

    openModal=(id)=>{
        console.log("card", id)
        debugger
        this.setState({
            open:true, 
            id:id
        })
        this.props.selected(id)
    }

    closeModal=()=>{
        this.setState({open:false, id:null})
    }

    render() {
        return (
            <div style={{margin:'1em'}}>
                <Card id={this.props.data.jobId} value={this.props.data.jobId} color="orange" onClick={()=>this.openModal(this.props.data.jobId)}>
                    <Card.Content id={this.props.data.jobId}>
                        <Card.Header>{this.props.data.name}</Card.Header>
                        <Card.Meta>
                            <Header color='teal' size='small'>{this.props.data.companyName}</Header>
                        </Card.Meta>
                        <Card.Description>
                            {this.props.data.company!==null?
                                <><Header style={{margin:'1px'}}  color='teal' size='medium'>{this.props.data.company}</Header>
                                <Header style={{margin:'0'}} sub size='small'>{this.props.data.location}</Header></>
                            :null}
                        <Header style={{margin:'0'}} sub color='orange' size='small'>Salary: Rs.{this.props.data.salary}</Header>
                            {`${this.props.data.description.substring(0,60)}...`}
                        </Card.Description>
                    </Card.Content>
                </Card>
                {/* <JobDataModal id={this.state.id} applied={this.props.applied===true?true:false} open={this.state.open} close={this.closeModal}/> */}
            </div>
        )
    }
}

export default Jobcard
