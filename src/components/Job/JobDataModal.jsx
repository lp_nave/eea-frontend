import React, { Component } from 'react'
import { Modal, Header, Button, Dimmer, Loader, Divider, Grid, GridColumn, TransitionablePortal, Segment, Message } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { JobActions, UserProfileActions } from '../../actions';
import moment from 'moment';

export class JobDataModal extends Component {

    constructor(props){
        super(props);
        this.state={
            id:null,

            status:null,
            openPortal:false
        }
    }

    static getDerivedStateFromProps(nextProps,prevState){
        if(nextProps.id!==null && nextProps.id!== prevState.id){
            nextProps.jobActions.getJob(nextProps.id)
            return {
                id: nextProps.id
            }
        }
        if(nextProps.status!==null){
            if(nextProps.status===true){
                return{
                    status:"success",
                    openPortal:true
                }
            }else if(nextProps.status===false){
                return{
                    status:"fail",
                    openPortal:true
                }
            }
            else return null
        }
        else{ 
            return null
        }
    }

    apply=(id)=>{ 
        this.props.userActions.applyForJob(id)
    }

    closeModal=()=>{
        this.setState({
            openPortal:false, 
            id:null,

        })
        this.props.userActions.clearUser()
        this.props.close()
    }

    // handleClose=()=>{
    //     this.setState({openPortal:false, status:null})
    //     this.props.jobActions.clearJob()
    //     this.props.close()
    // }

    render() {
        return (
            <Modal open={this.props.open} onClose={this.closeModal}>
                {this.props.data?
                <>
                    <Modal.Header>{this.props.data.name}</Modal.Header>
                    <Modal.Content scrolling>
                        <Grid>
                            <GridColumn width='10'>
                                <Header style={{margin:'1px'}} sub color='orange'>{moment(this.props.data.listedDate).format("Do MMM YYYY")}</Header>
                                <Header size='medium' color='teal' style={{margin:'1px'}}>{this.props.data.company}</Header>
                            </GridColumn>
                            <GridColumn width='6'>
                                <Header textAlign='right' size='medium' style={{margin:'1px'}}>{this.props.data.location}</Header>
                            </GridColumn>
                        </Grid>
                        <Modal.Description>
                            <Header color='grey' size='small'>{this.props.data.type}</Header>
                            <p size='small'>{this.props.data.description}</p>
                            <Divider horizontal >Requirements</Divider>
                            <Header size='small'>{this.props.data.requirements}</Header>
                            <Header color='orange' textAlign='right'>Salary : LKR {this.props.data.salary}</Header>
                        </Modal.Description>

                        {this.state.openPortal?
                        <>
                        {this.state.status==="success"?
                        <Message
                            positive
                            header="Success"
                            content="Successfully sent your CV to the company"
                        />
                        :
                        <Message
                            negative
                            header="Fail"
                            content="Failed to send your CV to the company"
                        />

                        }
                        </>
                        :null}
                    </Modal.Content>
                    <Modal.Actions>
                        {localStorage.getItem('role')==='company'||this.props.applied===true? null
                        :
                        <Button onClick={()=>{this.apply(this.props.data.jobId)}} id={this.props.data.jobId}  color='orange'>Apply</Button>
                        }
                    </Modal.Actions>
                    </>
                :
                <Dimmer active>
                    <Loader>Loading</Loader>
                </Dimmer>
                }
                {/* <TransitionablePortal open={this.state.openPortal} onClose={this.handleClose} transition={{animation:"fly up", duration:500}}>
                    <Segment color='orange' style={{ backgroundColor:'lightgrey',left: '40%', position: 'fixed', bottom: '5%', zIndex: 1000 }}>
                    {this.state.status==="success"?
                        <>
                            <Header color='green'>Success!</Header>
                            <p>Successfully sent your CV to the company</p>
                        </>
                    :null}
                    {this.state.status==='fail'?
                        <>
                            <Header color='red'>Failed!</Header>
                            <p>Failed to send your CV please upload CV</p>
                            <p>and try again</p>
                        </>
                    :null}
                    <Button fluid onClick={this.handleClose} content="Close" negative/>
                    </Segment>
                </TransitionablePortal> */}
            </Modal>
            
        )
    }
}

function mapDispatchToProps(dispatch){
    return{
        userActions: bindActionCreators(UserProfileActions,dispatch),
        jobActions: bindActionCreators(JobActions,dispatch),
    }
}

function mapStateToProps(state){
    return {
        data:state.Jobs.jobData,
        status:state.UserProfile.application
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(JobDataModal)
