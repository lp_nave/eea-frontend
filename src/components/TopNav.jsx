import React, { Component } from 'react'
import {Container,Divider,Dropdown,Grid,Header,Image,List,Menu,Segment, Icon,} from 'semantic-ui-react'
import { withRouter } from 'react-router-dom'

export class TopNav extends Component {

    logout=()=>{
        localStorage.removeItem('jwt')
        localStorage.removeItem('email')
        localStorage.removeItem('role')
        this.props.history.push('/Login')
    }

    render() {
        return (
            <div>
                <Menu fixed='top' inverted>
                    <Container>
                        <Menu.Item as='a' href='/HomePage' header>
                            <Image size='small' src='/homepagelogo.png' style={{ marginRight: '1.5em' }} />
                        </Menu.Item>
                        <Menu.Item as='a' href={localStorage.getItem('role')==='client'?'/UserHomePage':'/CompanyHomePage'}>Home</Menu.Item>
                        <Menu.Item as='a' href="/Jobs">Jobs</Menu.Item>
                        <Menu.Item as='a' href='/AllCompanies'>Companies</Menu.Item>

                        {/* <Dropdown item simple text='Dropdown'>
                        <Dropdown.Menu>
                            <Dropdown.Item>List Item</Dropdown.Item>
                            <Dropdown.Item>List Item</Dropdown.Item>
                            <Dropdown.Divider />
                            <Dropdown.Header>Header Item</Dropdown.Header>
                            <Dropdown.Item>
                            <i className='dropdown icon' />
                            <span className='text'>Submenu</span>
                            <Dropdown.Menu>
                                <Dropdown.Item>List Item</Dropdown.Item>
                                <Dropdown.Item>List Item</Dropdown.Item>
                            </Dropdown.Menu>
                            </Dropdown.Item>
                            <Dropdown.Item>List Item</Dropdown.Item>
                        </Dropdown.Menu>
                        </Dropdown> */}
                        <Menu.Item position='right' as='a' href={localStorage.getItem('role')==='client'?'/UserProfile':'/CompanyProfile'}>
                            <Icon circular name='user'/>Profile
                        </Menu.Item>
                        <Menu.Item onClick={this.logout}>
                        <Icon circular name='power off'/>
                            Log Out
                        </Menu.Item>
                    </Container>
                    </Menu>

            </div>
        )
    }
}

export default withRouter(TopNav)
