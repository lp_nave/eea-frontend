import React, { Component } from 'react'
import { Form, Input, Header, Segment, Button, Label, Message } from 'semantic-ui-react'

import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux'
import {authActions} from '../actions'

export class CompanySignUp extends Component {

    constructor(props){
        super(props);
        this.state={
            email:null,
            companyName:null,
            address:null,
            location:null,
            contactNumber:null,
            description:null,
            noOfEmployees:0,
            noOfJobs:0,
            password:null,
            repassword:null,
            showMsg:false,
            error:false
        }
    }

    static getDerivedStateFromProps(nextProps,prevState){
        debugger
        if(nextProps.isAuth===true){
            nextProps.history.push("/CompanyHomePage")
        } 
        if(nextProps.isAuth===false && prevState.isAuth!==nextProps.isAuth){
            return{
                showMsg:true,
                isAuth:nextProps.isAuth
            }
        }
        else{
            return null
        }
    }

    handleEmail=(e,{value})=>{
        this.setState({email:value})
    }
    handleCompanyName=(e,{value})=>{
        this.setState({companyName:value})
    }
    handleAddress=(e,{value})=>{
        this.setState({address:value})
    }
    handleCity=(e,{value})=>{
        this.setState({location:value})
    }
    handlePhone=(e,{value})=>{
        this.setState({contactNumber:value})
    }
    handleDescription=(e,{value})=>{
        this.setState({description:value})
    }
    handleEmployees=(e,{value})=>{
        this.setState({noOfEmployees:value})
    }
    handleJobs=(e,{value})=>{
        this.setState({noOfJobs:value})
    }
    handlePassword=(e,{value})=>{
        this.setState({password:value})
    }
    handleRepassword=(e,{value})=>{
        this.setState({repassword:value})
    }

    registerCompany=()=>{
        console.log("state", this.props, this.state)
        if(this.state.password!==this.state.repassword){
            this.setState({error:true})
        }else{
            debugger
            this.props.registerCompany(this.state)
        }
    }

    onDismiss=()=>{
        this.setState({showMsg:false})
    }

    render() {
        return (
            <div>
                <Form onSubmit={this.registerCompany} size='large'>
                    <Segment stacked >
                    <div style={{maxHeight:'400px',overflow:'auto', padding:'1em'}}>
                        <Header>Sign up your Company</Header>
                            
                        <Form.Input required type='email' onChange={this.handleEmail} focus fluid icon='at' iconPosition='left' placeholder='Company E-mail address' />   
                        <Form.Input required onChange={this.handleCompanyName} focus fluid placeholder='Company Name' />
                        <Form.Input required onChange={this.handleAddress} focus fluid placeholder='Company Address' />
                        <Form.Input required onChange={this.handleCity} focus fluid placeholder='City' />
                        <Form.Input required type='number' onChange={this.handlePhone} focus fluid icon='phone' iconPosition='left' placeholder='Contact Number' />
                        <Header size="small" color='orange'>Tell us about your company</Header>
                        <Form.Input required onChange={this.handleDescription} placeholder='Description'/>
                        <Form.Input required type="number" onChange={this.handleEmployees} focus fluid icon='users' iconPosition='left' placeholder='Number of employees'/>
                        <Form.Input required type="number" onChange={this.handleJobs} focus fluid icon='briefcase' iconPosition='left' placeholder='Number of jobs'/>
                        <Header size="small" color='orange'>Password</Header>
                        <Form.Input required onChange={this.handlePassword} focus fluid icon='lock' iconPosition='left' placeholder='Password' type='password'/>
                            {this.state.error?
                                <Label prompt color='red' pointing='below'>
                                    The passwords do not match
                                </Label>
                            :null}
                        <Form.Input required onChange={this.handleRepassword} focus fluid icon='lock' iconPosition='left' placeholder='Re enter Password' type='password'/>

                        {this.state.showMsg?
                        <Message
                            onDismiss={this.onDismiss}
                            negative
                            header={"Error"}
                            content={this.props.isAuthError}
                        />
                        :null}

                        <Button type='submit' inverted color='orange' fluid size='large'>
                            Sign up
                        </Button>
                    </div>
                    </Segment>
                </Form>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        ...state.Authenticate,
        isAuth:state.Authenticate.isAuthenticatedCompany,
        isAuthError:state.Authenticate.companyData,
    }
}

export default withRouter(connect(mapStateToProps, authActions) (CompanySignUp))
