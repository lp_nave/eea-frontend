import React, { Component } from 'react'
import { Segment, Input, Form, Button } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { CompanyActions, JobActions } from '../actions';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

export class Searchbar extends Component {

    constructor(props){
        super(props);
        this.state={
            searchkey:null,
            location:null,
        }
        this.handleSearchkey = this.handleSearchkey.bind(this)
        this.handleLocation = this.handleLocation.bind(this)
    }

    async handleSearchkey(e,{value}){
        if(value===''){
            await this.setState({searchkey:null})
        }else{
            await this.setState({searchkey:value})
        }
        // await this.setState({searchkey:value})
        this.refreshData()
    }

    componentDidMount(){

        console.log("loc", this.props.location.search)
        debugger
        if(this.props.location.search!==""){
            debugger
            this.setState({
                location:this.props.location.search.substring(5)
            })
        }
        // if(this.props.preload){
        //     this.setState({
        //         location:this.props.preload
        //     })
        // }
    }

    async handleLocation(e,{value}){
        if(value===''){
            await this.setState({location:null})
        }else{
            await this.setState({location:value})
        }

        this.refreshData()
        // await this.setState({location:value})
    }

    refreshData=()=>{
        if(this.state.searchkey===null && this.state.location==null){
            this.props.refresh()
        }
    }

    handleClick=(e)=>{
        if(this.props.type==='company'){
            this.props.companyActions.searchCompanies(this.state)
        }else if(this.props.type==='jobs'){
            this.props.jobActions.searchJobs(this.state)
        }
    }

    render() {
        return (
            <div>
               <Segment>
                   <Form>
                        <Form.Group widths="equal">
                            <Form.Input value={this.state.searchkey} onChange={this.handleSearchkey} fluid icon ='search' iconPosition='left' placeholder={this.props.job?'Job Title':'Company'}/>
                            <Form.Input value={this.state.location} onChange={this.handleLocation} fluid icon ='point' iconPosition='left' placeholder='Location'/>
                            <Button 
                            disabled={this.state.location===null && this.state.searchkey ===null? true:false} 
                            fluid 
                            color='teal'
                            onClick={this.handleClick}
                            >Search</Button>
                        </Form.Group>
                   </Form>
                </Segment> 
            </div>
        )
    }
}
function mapDispatchToProps(dispatch){
    return{
        jobActions: bindActionCreators(JobActions,dispatch),
        companyActions: bindActionCreators(CompanyActions,dispatch),
    }
}



export default withRouter(connect(null,mapDispatchToProps) (Searchbar))
