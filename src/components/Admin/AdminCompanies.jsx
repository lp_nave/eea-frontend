import React, { Component } from 'react'
import {AgGridReact} from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { Button, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { AdminActions } from '../../actions';

export class AdminCompanies extends Component {
    constructor(props){
        super(props);
        this.state={
            columnDefs: [
                { headerName: "ID", field: "companyId", filter:"agNumberColumnFilter",cellRenderer: "agGroupCellRenderer",
                cellRendererParams: { checkbox: true } },
                { headerName: "Name", field: "companyName" , filter:"agNumberColumnFilter"},
                { headerName: "Location", field: "location" , filter:"agNumberColumnFilter"},
                { headerName: "Rating", field: "rating" , filter:"agNumberColumnFilter"},
                { headerName: "Image Name", field: "imagePath" , filter:"agNumberColumnFilter"},
                { headerName: "No of Vacancies", field: "jobs" , filter:"agNumberColumnFilter"},
            ],
              rowData:null 
        }
    }

    componentDidMount(){
        this.props.adminCompanies();
    }

    static getDerivedStateFromProps(nextProps,prevState){
        if(nextProps.companies && JSON.stringify(nextProps.companies)!== JSON.stringify(prevState.rowData)){
            return{
                rowData:nextProps.companies
            }
        }
        else return null
    }

    onGridReady=(params)=>{
        this.gridApi = params.api
    }

    handleSelection=(e)=>{
        let select= this.gridApi.getSelectedRows();
        this.setState({
            selected:select[0]
        })
        console.log("selected", this.state.selected)
        debugger
    }


    handleDownload=()=>{
        this.props.adminDownloadCompanies();
    }

    handleDelete=()=>{
        let id = this.state.selected.companyId
        this.props.deleteCompany(id);
    }

    render() {
        return (
            <div className="ag-theme-balham" style={{height: '400px', width: '100%', textAlign:'center',display:'inline-block'} }>
                <AgGridReact
                    columnDefs={this.state.columnDefs}
                    rowData={this.state.rowData}
                    onSelectionChanged={this.handleSelection}
                    onGridReady={this.onGridReady}
                    rowSelection={"single"}
                    enableColResize={true}
                    >
                </AgGridReact>
                {/* <Button.Group> */}
                    <Button onClick={this.handleDelete} style={{margin:'1em'}} color='black'><Icon name='trash'/>Delete</Button> 
                    <Button onClick={this.handleDownload} style={{margin:'1em'}} color='orange'><Icon name='table'/> CSV</Button> 
                {/* </Button.Group> */}
            </div>
        )
    }
}

function mapStateToProps(state){
    return{
        companies:state.Admin.companies
    }
}

export default connect(mapStateToProps,AdminActions)(AdminCompanies)
