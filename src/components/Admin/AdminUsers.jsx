import React, { Component } from 'react'
import {AgGridReact} from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { Button, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { AdminActions } from '../../actions';

export class AdminUsers extends Component {
    constructor(props){
        super(props);
        this.state={
            columnDefs: [
                { headerName: "Email", field: "email", filter:"agNumberColumnFilter",cellRenderer: "agGroupCellRenderer",
                cellRendererParams: { checkbox: true } },
                { headerName: "First Name", field: "firstName" , filter:"agNumberColumnFilter"},
                { headerName: "Last Name", field: "lastName", filter:"agNumberColumnFilter" },
                { headerName: "Location", field: "location" , filter:"agNumberColumnFilter"},
                { headerName: "Contact", field: "contactNumber", filter:"agNumberColumnFilter" },
                { headerName: "Applied Jobs", field: "sumJobs", filter:"agNumberColumnFilter" },
            ],
              rowData: null
        }
    }

    componentDidMount(){
        this.props.adminClients()
    }

    static getDerivedStateFromProps(nextProps,prevState){
        if(nextProps.clients&& JSON.stringify(nextProps.clients)!== JSON.stringify(prevState.rowData)){
            return{
                rowData: nextProps.clients
            }
        }
        else return null
    }

    onGridReady=(params)=>{
        this.gridApi = params.api
    }

    handleSelection=(e)=>{
        let select= this.gridApi.getSelectedRows();
        this.setState({
            selected:select[0]
        })
        console.log("selected", this.state.selected)
        debugger
    }

    handleDownload=()=>{
        console.log(" the props", this.props)
        this.props.adminDownloadClients()
    }

    handleDelete=()=>{
        let id = this.state.selected.email
        this.props.deleteClient(id)
    }

    render() {
        return (
            <div className="ag-theme-balham" style={{height: '400px', width: '100%', textAlign:'center',display:'inline-block'} }>
                <AgGridReact
                    columnDefs={this.state.columnDefs}
                    rowData={this.state.rowData}
                    onSelectionChanged={this.handleSelection}
                    onGridReady={this.onGridReady}
                    rowSelection={"single"}
                    enableColResize={true}
                    >
                </AgGridReact>
                {/* <Button.Group> */}
                    <Button onClick={this.handleDelete} style={{margin:'1em'}} color='black'><Icon name='trash'/>Delete</Button> 
                    <Button onClick={this.handleDownload} style={{margin:'1em'}} color='orange'><Icon name='table'/>CSV</Button> 
                {/* </Button.Group> */}
            </div>
        )
    }
}

function mapStateToProps(state){
    return{
        clients:state.Admin.clients
    }
}

export default connect(mapStateToProps,AdminActions)(AdminUsers)
