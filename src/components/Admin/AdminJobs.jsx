import React, { Component } from 'react'
import {AgGridReact} from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { Button, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { AdminActions } from '../../actions';
import moment from 'moment';

export class AdminJobs extends Component {
    constructor(props){
        super(props);
        this.state={
            columnDefs: [
                { headerName: "Job ID", field: "jobId" ,filter:"agNumberColumnFilter",cellRenderer: "agGroupCellRenderer",
                cellRendererParams: { checkbox: true }},
                { headerName: "Title", field: "name" , filter:"agNumberColumnFilter"},
                { headerName: "Company", field: "company" , filter:"agNumberColumnFilter"},
                { headerName: "Salary", field: "salary" , filter:"agNumberColumnFilter"},
                { headerName: "Location", field: "location" , filter:"agNumberColumnFilter"},
                { headerName: "Listed Date", field: "listedDate",
                valueGetter:(params)=>{
                    params.api.sizeColumnsToFit()
                    return moment(params.data.listedDate).format("LLL")
                }
            },
            ],
              rowData:null 
        }
    }

    componentDidMount(){
        this.props.adminJobs()
    }

    static getDerivedStateFromProps(nextProps,prevState){
        if(nextProps.jobs && JSON.stringify(nextProps.jobs)!== JSON.stringify(prevState.rowData)){
            return{
                rowData: nextProps.jobs
            }
        }
        else return null
    }

    onGridReady=(params)=>{
        this.gridApi = params.api
    }

    handleSelection=(e)=>{
        let select= this.gridApi.getSelectedRows();
        this.setState({
            selected:select[0]
        })
        console.log("selected", this.state.selected)
        debugger
    }

    handleDownload=()=>{
        debugger
        this.props.adminDownloadJobs();
    }

    handleDelete=()=>{
        let id = this.state.selected.jobId
        this.props.deleteJobs(id)

    }

    render() {
        return (
            <div className="ag-theme-balham" style={{height: '400px', width: '100%', textAlign:'center',display:'inline-block'} }>
                <AgGridReact
                    columnDefs={this.state.columnDefs}
                    rowData={this.state.rowData}
                    onSelectionChanged={this.handleSelection}
                    onGridReady={this.onGridReady}
                    rowSelection={"single"}
                    enableColResize={true}
                    >
                </AgGridReact>
                {/* <Button.Group> */}
                    <Button onClick={this.handleDelete} style={{margin:'1em'}} color='black'><Icon name='trash'/>Delete</Button> 
                    <Button onClick={this.handleDownload} style={{margin:'1em'}} color='orange'><Icon name='table'/> CSV</Button> 
                {/* </Button.Group> */}
            </div>
        )
    }
}

function mapStateToProps(state){
    return{
        jobs:state.Admin.jobs
    }
}

export default connect(mapStateToProps,AdminActions)(AdminJobs)
