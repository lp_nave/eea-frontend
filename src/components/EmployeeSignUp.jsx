import React, { Component } from 'react'
import { Form, Input, Header, Segment, Button, Label, Message } from 'semantic-ui-react'

import {authActions} from '../actions'
// import {Authenticate} from '../reducers'

import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'

export class EmployeeSignUp extends Component {

    constructor(props){
        super(props)
        this.state={
            email:null,
            firstName:null,
            lastName:null,
            contactNumber:null,
            location:null,
            password:null,
            repassword:null,
            showMsg:false,
            error:false
        }
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.isAuth===true){
            debugger
            nextProps.history.push("/UserHomePage")
        }
        if(nextProps.isAuth===false && prevState.isAuth!==nextProps.isAuth){
            return{
                showMsg:true,
                isAuth:nextProps.isAuth
            }
        }
        else {
            return null
        }
    }

    handleEmail=(e,{value})=>{
        this.setState({email:value})
    }

    handleFirstName=(e,{value})=>{
        this.setState({firstName:value})
    }

    handleLastName=(e,{value})=>{
        this.setState({lastName:value})
    }
    handleContactNumber=(e,{value})=>{
        this.setState({contactNumber:value})
    }
    handleLocation=(e,{value})=>{
        this.setState({location:value})
    }

    handlePassword=(e,{value})=>{
        this.setState({password:value})
    }

    handleRePassword=(e,{value})=>{
        this.setState({repassword:value})
    }

    registerClient=(e)=>{
        console.log("actiojns",this.state)
        if(this.state.password!==this.state.repassword){
            this.setState({error:true})
        }else{
            this.props.registerClient(this.state)
        }
    }

    onDismiss=()=>{
        this.setState({showMsg:false})
    }


    render() {
        return (
            <div>
                <Form onSubmit={this.registerClient} size='large'>
                    <Segment stacked>
                    <div style={{maxHeight:'400px',overflow:'auto', padding:'1em'}}>
                        <Header>Sign up as a User</Header>
                        <Form.Input required type='email' onChange={this.handleEmail} focus fluid icon='at' iconPosition='left' placeholder='E-mail address' />   
                        <Form.Input required onChange={this.handleFirstName} focus fluid placeholder='First Name' />
                        <Form.Input required onChange={this.handleLastName}focus fluid placeholder='Last Name' />
                        <Form.Input required onChange={this.handleLocation}focus fluid placeholder='Location' />
                        <Form.Input required type='number' onChange={this.handleContactNumber}focus fluid icon='phone' iconPosition='left' placeholder='Contact Number' />
                        
                        <Form.Input required onChange={this.handlePassword}focus fluid icon='lock' iconPosition='left' placeholder='Password' type='password'/>
                            {this.state.error?
                                <Label prompt color='red' pointing='below'>
                                    The passwords do not match
                                </Label>
                            :null}
                        <Form.Input required onChange={this.handleRePassword}focus fluid icon='lock' iconPosition='left' placeholder='Re enter Password' type='password'/>

                        {this.state.showMsg?
                        <Message
                            onDismiss={this.onDismiss}
                            negative
                            header={"Error"}
                            content={this.props.isAuthError}
                        />
                        :null}
                        <Button type='submit'  inverted color='orange' fluid size='large'>
                            Sign up
                        </Button>
                        </div>
                    </Segment>
                </Form>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        ...state.Authenticate,
        isAuth:state.Authenticate.isAuthenticatedClient,
        isAuthError:state.Authenticate.userData,
    }
}

export default withRouter(connect(mapStateToProps, authActions) (EmployeeSignUp))
