import React, { Component } from 'react'
import { Form, Modal, Button, Header, TransitionablePortal, Segment, Label } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { CompanyProfileActions } from '../../actions'

export class EditCompanyModal extends Component {

    constructor(props){
        super(props);
        this.state={
            data:null,
            email:null,
            companyName:null,
            address:null,
            contactNumber: null,
            noOfEmployees: null,
            noOfJobs: null,
            description: null,
            location: null,
            password: null,
            repassword: null,
            
            status:null,
            openPortal:false
        }
    }

    componentDidMount(){
        this.props.getCompany();
    }

    static getDerivedStateFromProps(nextProps,prevState){
        // if(nextProps.company){
            if(JSON.stringify(nextProps.company) !== JSON.stringify(prevState.data)){
                console.log("comp", nextProps.company)
                return{
                    data:nextProps.company,
                    email:nextProps.company.email,
                    companyName:nextProps.company.companyName,
                    address:nextProps.company.address,
                    contactNumber: nextProps.company.contactNumber,
                    noOfEmployees: nextProps.company.noOfEmployees,
                    noOfJobs: nextProps.company.noOfJobs,
                    description: nextProps.company.description,
                    location: nextProps.company.location,  
                }
            }
            if(nextProps.isEdit!==null){
                if(nextProps.isEdit===true){
                    return{
                        status:"success",
                        openPortal:true
                    }
                }else if(nextProps.isEdit===false){
                    return{
                        status:"fail",
                        openPortal:true
                    }
                }
            }
        // }
        else return null
    }

    handleCompanyName=(e,{value})=>{
        this.setState({companyName:value})
    }
    handleAddress=(e,{value})=>{
        this.setState({address:value})
    }
    handleCity=(e,{value})=>{
        this.setState({location:value})
    }
    handlePhone=(e,{value})=>{
        this.setState({contactNumber:value})
    }
    handleDescription=(e,{value})=>{
        this.setState({description:value})
    }
    handleEmployees=(e,{value})=>{
        this.setState({noOfEmployees:value})
    }
    handleJobs=(e,{value})=>{
        this.setState({noOfJobs:value})
    }
    handlePassword=(e,{value})=>{
        this.setState({password:value})
    }
    handleRepassword=(e,{value})=>{
        this.setState({repassword:value})
    }

    handleEdits=()=>{
        if(this.state.password!==this.state.repassword){
            this.setState({error:true})
        }else{
            this.props.editCompany(this.state)
        }
    }

    handleClose=()=>{
        debugger
        this.props.clearStatusLog()
        this.setState({openPortal:false, status:null})
    }

    render() {
        return (
            <Modal open={this.props.open} onClose={this.props.close} >
                <Modal.Header>Edit Company Profile</Modal.Header>
                <Modal.Content scrolling>
                    <Modal.Description>
                        <Form onSubmit={this.handleEdits}>
                            <Form.Input value={this.state.email} required focus fluid icon='at' iconPosition='left' placeholder='Company E-mail address' />   
                            <Form.Input value={this.state.companyName} required onChange={this.handleCompanyName} focus fluid placeholder='Company Name' />
                            <Form.Input value={this.state.address} required onChange={this.handleAddress} focus fluid placeholder='Company Address' />
                            <Form.Input value={this.state.location} required onChange={this.handleCity} focus fluid placeholder='City' />
                            <Form.Input value={this.state.contactNumber} required onChange={this.handlePhone} focus fluid icon='phone' iconPosition='left' placeholder='Contact Number' />
                            <Header size="small" color='orange'>Your company details</Header>
                            <Form.TextArea value={this.state.description} required onChange={this.handleDescription} placeholder='Description'/>
                            <Form.Input value={this.state.noOfEmployees} required type="number" onChange={this.handleEmployees} focus fluid icon='users' iconPosition='left' placeholder='Number of employees'/>
                            <Form.Input value={this.state.noOfJobs} required type="number" onChange={this.handleJobs} focus fluid icon='briefcase' iconPosition='left' placeholder='Number of jobs'/>
                            <Header size="small" color='orange'>Password</Header>
                            <Header sub size="small" color='orange'>Enter only if you wish to change</Header>
                            <Form.Input onChange={this.handlePassword} focus fluid icon='lock' iconPosition='left' placeholder='Password' type='password'/>
                            {this.state.error?
                                <Label prompt color='red' pointing='below'>
                                    The passwords do not match
                                </Label>
                            :null}
                            <Form.Input onChange={this.handleRepassword} focus fluid icon='lock' iconPosition='left' placeholder='Re enter Password' type='password'/>

                            <Button color="orange" type='submit'>
                                Save Chnages
                            </Button>
                        </Form>
                    </Modal.Description>
                    <TransitionablePortal open={this.state.openPortal} onClose={this.handleClose} transition={{animation:"fly up", duration:500}}>
                        <Segment color='orange' style={{  backgroundColor:'lightgrey',left: '40%', position: 'fixed', bottom: '5%', zIndex: 1000 }}>
                        {this.state.status==="success"?
                            <>
                                <Header color='green'>Success!</Header>
                                <p>Your Company Profile has been succesfully updated</p>
                            </>
                        :null}
                        {this.state.status==='fail'?
                            <>
                                <Header color='red'>Failed!</Header>
                                <p>Failed to upadate your company profile.</p>
                                <p>Please try again</p>
                            </>
                        :null}
                        <Button fluid onClick={this.handleClose} content="Close" negative/>
                        </Segment>
                    </TransitionablePortal>
                </Modal.Content>
                {/* <Modal.Actions>
                </Modal.Actions> */}
            </Modal>
        )
    }
}

// function mapDispatchToProps(dispatch){
//     return{
//         companyProfileActions: bindActionCreators(CompanyProfileActions,dispatch),
//         // companyActions: bindActionCreators(CompanyActions,dispatch),
//     }
// }

function mapStateToProps(state){
    return {
        company:state.CompanyProfile.company,
        isEdit: state.CompanyProfile.isEdit,
        // departments:state.Company.departments,
    }
}

export default connect(mapStateToProps,CompanyProfileActions)(EditCompanyModal)
