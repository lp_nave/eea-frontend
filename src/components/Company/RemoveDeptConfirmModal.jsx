
import { Modal, Header, Button, Icon, TransitionablePortal, Segment } from 'semantic-ui-react'

import React, { Component } from 'react'
import { connect } from 'react-redux';
import { CompanyActions } from '../../actions';

export class RemoveDeptConfirmModal extends Component {

    constructor(props){
        super(props);
        this.state={

        }
    }

    delete=()=>{
        if(this.props.deleteId){
            this.props.deleteDepartment(this.props.deleteId)
        }
        this.props.close()
    }

    render() {
        return (
            <Modal basic size='small' open={this.props.open} onClose={this.props.close}>
                <Header icon='warning sign' color='orange' content='Deleting a Department' />
                <Modal.Content>
                <p>
                    Are you sure you want to delete a Department from your profile? All the vacancy listings under
                    that Department will be lost
                </p>
                </Modal.Content>
                <Modal.Actions>
                <Button color='red' inverted onClick={()=>{this.props.close()}}>
                    <Icon name='remove'/> No
                </Button>
                <Button color='green' inverted onClick={this.delete}>
                    <Icon name='checkmark' /> Yes
                </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}


export default connect(null, CompanyActions) (RemoveDeptConfirmModal)
