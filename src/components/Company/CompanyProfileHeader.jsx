import React, { Component } from 'react'
import { Segment, Container, Image, Menu, Header, Icon, Rating, TransitionablePortal, Button } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { CompanyProfileActions } from '../../actions';
import '../../../src/image.css'

export class CompanyProfileHeader extends Component {

    constructor(props){
        super(props);;
        this.state={
            // activeItem:"home"
            data:null,
            rating:null,
            id:null,
            openPortal:false
        }
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.data && JSON.stringify(nextProps.data)!==JSON.stringify(prevState.data)){
            console.log('nextPRops', nextProps.data)
            return{
                id:nextProps.data.id,
                rating:nextProps.data.rating,
                data:nextProps.data
            }

        }else return null
    }

    // handleItemClick=(e,{value})=>{
    //     this.setState({
    //         activeItem:value
    //     })
    // }

    handleRate=(e, {rating})=>{
        if(localStorage.getItem('role')==="client"){
            debugger
            this.setState({
                rating:rating
            })
            let obj={
                id:this.state.id,
                rate:rating
            }
            this.props.rate(obj)
        }
        else{
            debugger
            this.setState({
                openPortal:true
            })
        }
    }

    handleClose=()=>{
        this.setState({openPortal:false})
    }

    render() {
        console.log("rate issue", this.state.rating)
        return (
            <div style={{width:'90%', textAlign:'center', display:'inline-block'}}>
                <Segment
                 inverted
                //  textAlign='center'
                 style={{ 
                minHeight: 150, 
                // padding: '1em 0em',
                 width: "100%",
                //  height: '100%',
                //  display: 'inline-block',
                 backgroundImage: `url(${"/gradient.jpg"})`,
                 backgroundSize: 'cover',
                 marginBottom: 0,
                }}
                >  
                <Segment size='large' style={{
                    width: '150px',
                    // height: '150px',
                    textAlign: 'center',
                    display: 'inline-block',
                    zIndex: 100,
                    top: '5em',
                    right: '32em',
                    padding:0
                }}>
                    <Image rounded size='mini' verticalAlign='middle'
                    // ui={false}
                    // wrapped={true}
                    // className='image'
                        style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            overflow: 'hidden',
                            flexShrink: 0,
                            minWidth: '100%',
                            minHeight: '100%',
                        }}
                        src={this.props.data!==null && this.props.data.image!==null?
                            `data:image/${this.props.data.imagePath.slice((this.props.data.imagePath.lastIndexOf(".") - 1 >>> 0) + 2)};base64,${this.props.data.image}`
                           :'https://react.semantic-ui.com/images/wireframe/image.png'}
                        
                        
                    />
                </Segment>
                </Segment>
                
                <Segment style={{marginTop:0}}>
                    <div style={{marginTop:'1.5em'}}>
                        <Header style={{marginLeft:'10em'}} floated='left'>{this.props.data && this.props.data.companyName}</Header>
                        <Header sub color='orange' floated='left'>{this.props.data && this.props.data.location}</Header>
                        <div style={{float:'right'}}>
                            <Rating 
                            // disabled={localStorage.getItem('role')==='company'? true:false}
                            icon='star'
                            onRate={this.handleRate}
                            size='large'
                            maxRating={5}
                            defaultRating={this.state.rating}
                            />
                            <Header 
                            floated='left'
                            size='large'
                            color='orange'>
                                {this.props.data && this.props.data.rating}
                            </Header>
                        </div>
                    {/* <Segment > */}
                        <Menu inverted fluid borderless widths={4}>
                        <Menu.Item 
                           content={
                               <div>
                                   <Icon size='huge' name="briefcase"/>
                                   <Header color='orange' inverted style={{margin:0}}>{this.props.data && this.props.data.jobs}</Header>
                                   <p >JOBS</p>
                               </div>
                        }
                        />
                        <Menu.Item
                            content={
                                <div>
                                    <Icon size='huge' name="users"/>
                                    <Header color='orange' inverted style={{margin:0}}>{this.props.data && this.props.data.employee}</Header>
                                    <p >EMPLOYEES</p>
                                </div>
                            }
                        />
                        <Menu.Item
                           content={
                                <div>
                                    <Icon size='huge' name="star half empty"/>
                                    <Header color='orange' inverted style={{margin:0}}>{this.props.data && this.props.data.vacancies}</Header>
                                    <p>VACANCIES</p>
                                </div>
                            }
                        />
                        <Menu.Item
                           content={
                                <div>
                                    <Icon size='huge' name="factory"/>
                                    <Header color='orange' inverted style={{margin:0}}>{this.props.data && this.props.data.departments}</Header>
                                    <p>DEPARTMENTS</p>
                                </div>
                            }
                        />
                        </Menu>
                    </div>
                </Segment>
                <TransitionablePortal open={this.state.openPortal} onClose={this.handleClose} transition={{animation:"fly up", duration:500}}>
                        <Segment color='orange' style={{ backgroundColor:'lightgrey',left: '40%', position: 'fixed', bottom: '5%', zIndex: 1000 }}>
                                <Header color='red'>Sorry!</Header>
                                <p>Companies arent allowed to rate other themselves or companies</p>
                        <Button fluid onClick={this.handleClose} content="Close" negative/>
                        </Segment>
                </TransitionablePortal>
            </div>
        )
    }
}

export default connect(null,CompanyProfileActions)(CompanyProfileHeader)
