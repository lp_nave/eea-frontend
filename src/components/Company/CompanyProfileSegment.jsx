import React, { Component } from 'react'
import { Segment, Image, Header, Divider, Button, List, Message } from 'semantic-ui-react'
import DepartmentModal from './DepartmentModal'
import JobVacancyModal from '../Job/JobVacancyModal';

import { connect } from 'react-redux';
import {CompanyActions} from '../../actions'
import ImageUploader from '../ImageUploader';
import '../../../src/image.css'

export class CompanyProfileSegment extends Component {

    constructor(props){
        super(props);
        this.state={
            departmemt:false,
            vacancyModal:false,
            image:false
        }
    }

    deparmentBtn=()=>{
        this.setState({departmemt:true})
    }

    handleDepartmentClose=()=>{
        this.setState({departmemt:false})
    }

    handleVacancyBtn=()=>{
        this.setState({vacancyModal:true})
    }

    handleVacancyModalClose=()=>{
        this.setState({vacancyModal:false})
    }

    saveDepartment=(dept)=>{
        console.log("saa", dept, this.props)
        this.setState({departmemt:false})
        this.props.addDepartment(dept)
    }

    openImage=()=>{
        this.setState({image:true})
    }

    closeImage=()=>{
        this.setState({image:false})
    }


    render() {
        return (
            <div>
                 <Segment>
                    <Image 
                     verticalAlign='middle'
                     onClick={this.openImage}
                     size='medium'
                     className='img'
                     ui={false}
                     wrapped={true}
                     label={{ as: 'a',size:'tiny',color: 'red', corner: 'right', icon: 'edit', onClick:this.openImage }}
                     src={
                        this.props.image!==null?
                        `data:image/${this.props.imagePath.slice((this.props.imagePath.lastIndexOf(".") - 1 >>> 0) + 2)};base64,${this.props.image}`
                       :'https://react.semantic-ui.com/images/wireframe/image.png'}
                    
                    />
                    <Header>{this.props.name}</Header>
                    <Header color='orange'size="large" sub>{this.props.location}</Header>
                    <Divider horizontal/>
                    <Button fluid style={{margin:'5px'}} color='black' onClick={this.deparmentBtn}>Add Department</Button>
                    <Button
                        disabled={this.props.departments && this.props.departments.length!=0?false:true}  
                        fluid 
                        style={{margin:'5px'}} 
                        color='orange'
                        onClick={this.handleVacancyBtn}>
                            Add job vacancy
                    </Button>
                    <Button 
                        fluid
                        disabled={this.props.jobs && this.props.jobs.length!=0?false:true}
                        style={{margin:'5px'}} 
                        color='grey' as='a' 
                        href='/Applicants'>
                            Applicants
                    </Button>
                </Segment>
                <Segment>
                    <Header>Your Departments</Header>
                    {this.props.departments && this.props.departments.length!=0?
                        <List>
                            {this.props.departments.map((item,index)=>{
                                return <List.Item key={index}>{item.departmentName}</List.Item>
                            
                            })}
                        </List>
                    :
                    <Message error>
                        <Message.Header>No Departments</Message.Header>
                        <p>
                        You have not added any departments/teams into your company profile.
                        Here's an example: "Systems Team", "Finance Department"
                        </p>
                    </Message>
                     }
                </Segment>

                <JobVacancyModal edit={false} open={this.state.vacancyModal} close={this.handleVacancyModalClose}/>
                <DepartmentModal api={this.saveDepartment} openDeparmentModal={this.state.departmemt} closeDepartmentModal={this.handleDepartmentClose}/>
                <ImageUploader open={this.state.image} close={this.closeImage}/>
            </div>
        )
    }
}

export default connect(null,CompanyActions )(CompanyProfileSegment)
