import React, { Component } from 'react'
import { Form, Input, Modal, Button, TransitionablePortal, Segment, Header } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { CompanyActions } from '../../actions';

export class DepartmentModal extends Component {
    constructor(props){
        super(props);
        this.state={
            deptName:'',
            status:null,
            openPortal:false,
        }
    }

    static getDerivedStateFromProps(nextProps,prevState){
        if(nextProps.isDepartmentAdded!==null){
            if(nextProps.isDepartmentAdded){
                return{
                    status:'success',
                    openPortal:true,
                }
            }
            else if(nextProps.isDepartmentAdded){
                return{
                    status:'fail',
                    openPortal:true,
                }
            }
        }else return null
    }

    handleInput=(e,{value})=>{
        this.setState({deptName:value})
        console.log(this.state.deptName)
    }

    handleApi=()=>{
        this.setState({deptName:''})
        this.props.api(this.state.deptName)
    }

    handleClose=()=>{
        debugger
        this.props.clearDepartmentStatus()
        this.setState({openPortal:false, status:null})
        this.props.closeDepartmentModal()
    }

    render() {
        return (
            <>
            <Modal open={this.props.openDeparmentModal} onClose={this.props.closeDepartmentModal} >
                <Modal.Header>Add Department</Modal.Header>
                <Modal.Content scrolling>
                    <Modal.Description>
                        <Form>
                            <Input onChange={this.handleInput} label='Department Name' labelPosition='left'/>
                        </Form>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                <Button disabled={this.state.deptName===''? true:false} onClick={this.handleApi} color="orange">
                    Save
                </Button>
                </Modal.Actions>
            </Modal>
            <TransitionablePortal open={this.state.openPortal} onClose={this.handleClose} transition={{animation:"fly up", duration:500}}>
            <Segment color='orange' style={{ backgroundColor:'lightgrey' ,left: '40%', position: 'fixed', bottom: '5%', zIndex: 1000 }}>
            {this.state.status==="success"?
                <>
                    <Header color='green'>Success!</Header>
                    <p>Successfully added a Department</p>
                </>
            :null}
            {this.state.status==='fail'?
                <>
                    <Header color='red'>Failed!</Header>
                    <p>Failed to add department</p>
                    <p>Please try again</p>
                </>
            :null}
            <Button fluid onClick={this.handleClose} content="Close" negative/>
            </Segment>
            </TransitionablePortal>
            </>
        )
    }
}
function mapStateToProps(state){
    return {
        isDepartmentAdded:state.Company.isDepartmentAdded,
    }
}

export default connect(mapStateToProps,CompanyActions) (DepartmentModal)
