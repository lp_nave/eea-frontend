import React, { Component } from 'react'
import { Card, Button, Message, TransitionablePortal, Segment, Header } from 'semantic-ui-react'
import JobVacancyModal from '../Job/JobVacancyModal'
import moment from 'moment'
import { connect } from 'react-redux';
import { JobActions } from '../../actions';

export class Vacancy extends Component {

  constructor(props){
    super(props);
    this.state={
        vacancyModal:false,
        id:null,

        status:null,
        openPortal:false
    }
  }

  static getDerivedStateFromProps(nextProps,prevState){
    if(nextProps.deleteStatus!==null){
      if(nextProps.deleteStatus===true){
          return{
              status:"success",
              openPortal:true
          }
      }else if(nextProps.deleteStatus===false){
          return{
              status:"fail",
              openPortal:true
          }
      }
    }else return null
  }

  handleVacancyBtn=(id)=>{
    console.log('testing',id)
    this.setState({vacancyModal:true, id:id})
}

handleVacancyModalClose=()=>{
    this.setState({vacancyModal:false})
}

deleteJob=(e,{id})=>{
  debugger
  this.props.deleteJob(id)
}

handleClose=()=>{
  debugger
  this.props.clearJob()
  this.setState({openPortal:false, status:null})
  // this.props.close()
}
    
    render() {
        return (
            <div>
                {this.props.jobs && this.props.jobs.length!=0?
                
                <Card.Group centered>
                  {this.props.jobs.map((item, index)=>{
                    if(index<3){
                    return(
                      <Card color='orange' key={item.jobId}>
                        <Card.Content>
                          <Card.Header>{item.name}</Card.Header>
                          <Card.Meta>{moment(item.listedDate).format("Do MMM YYYY")}</Card.Meta>
                          <Card.Description>
                            {item.type}
                          </Card.Description>
                        </Card.Content>
                        {this.props.company==true?
                          <Card.Content extra>
                            <div className='ui two buttons'>
                              <Button inverted color='green' id={item.jobId} onClick={()=>{this.handleVacancyBtn(item.jobId)}}>
                                Edit
                              </Button>
                              <Button inverted color='red' id={item.jobId} onClick={this.deleteJob}>
                                Remove
                              </Button>
                            </div>
                          </Card.Content>
                        :null}
                      </Card>
                    )
                    }
                    else return null
                  })}
                </Card.Group>

                :
                <Message error>
                    <Message.Header>No Jobs added yet</Message.Header>
                    <p>
                    You have not listed an jobs yet, Click "Add Job Vacancy" to add a job vacancy
                    </p>
                </Message>
                }
                
                <TransitionablePortal open={this.state.openPortal} onClose={this.handleClose} transition={{animation:"fly up", duration:500}}>
                        <Segment color='orange' style={{  backgroundColor:'lightgrey',left: '40%', position: 'fixed', bottom: '5%', zIndex: 1000 }}>
                        {this.state.status==="success"?
                            <>
                                <Header color='green'>Success!</Header>
                                <p>Your Company Profile has been succesfully updated</p>
                            </>
                        :null}
                        {this.state.status==='fail'?
                            <>
                                <Header color='red'>Failed!</Header>
                                <p>Failed to upadate your company profile.</p>
                                <p>Please try again</p>
                            </>
                        :null}
                        <Button fluid onClick={this.handleClose} content="Close" negative/>
                        </Segment>
                </TransitionablePortal>


            <JobVacancyModal edit={true} id={this.state.id} open={this.state.vacancyModal} close={this.handleVacancyModalClose}/>
            </div>
        )
    }
}


function mapStateToProps(state){
  return {
      deleteStatus:state.Jobs.deleteStatus,
  }
}

export default connect(mapStateToProps,JobActions) (Vacancy)
