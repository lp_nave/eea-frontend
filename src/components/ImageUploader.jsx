import React, { Component } from 'react'
import { Modal, Input, Image, Header, Button } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { CVActions } from '../actions';
import { bindActionCreators } from 'redux';

export class ImageUploader extends Component {

    constructor(props){
        super(props);
        this.state={
            image:'https://react.semantic-ui.com/images/avatar/large/rachel.png',
            disabled:true,
            selectedFile:null
        }
        this.image= this.image.bind(this);
        this.handleUpload = this.handleUpload.bind(this)
    }

    componentDidMount(){
        console.log(this.props)
    }


    async image(e){
        debugger
        let a = e.target.files[0]
        const toBase64 = file => new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });

        let preview = await toBase64(a);
        console.log(preview);
        debugger

        this.setState({
            selectedFile:a,
            disabled:false,
            image : preview
        })
    }

    handleUpload(){
        const data = new FormData()
        data.append('file', this.state.selectedFile)
        console.log(this.props)
        debugger
        this.props.uploadImage(data);
    }



    render() {
        return (
        
            <Modal open={this.props.open} onClose={this.props.close}>
                <Modal.Header>Select a Photo</Modal.Header>
                    <Modal.Content image>
                    <Image  size='small' src={this.state.image}/>
                    <Modal.Description>
                        <Header>Select your Profile picture</Header>
                        <Header size='small' color='red'>Max file size is 5MB</Header>
                        <Input type='file' accept="image/x-png,image/gif,image/jpeg" onChange={this.image}/>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button  disabled={this.state.disabled} onClick={this.handleUpload}color='orange' >Change Picture</Button>
                </Modal.Actions>   
            </Modal>
        )
    }
}

function mapStateToProps(state){
    return {
        isUploaded:state.CV.image,
        // companyData:state.Company.randomCompanies,
    }
}

export default connect(mapStateToProps,CVActions)(ImageUploader)
