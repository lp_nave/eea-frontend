import React, { Component } from 'react'
import { Segment, Image, Header, Divider, Button, Input, Message, Label } from 'semantic-ui-react'
import UploadCVModel from './UploadCVModel'
import ImageUploader from '../ImageUploader';
import '../../../src/image.css'

export class UserProfileSegment extends Component {

    constructor(props){
        super(props);
        this.state={
            open:false,
            image:false
        }
    }

    closeUpload=(e)=>{
        this.setState({open:false})
    }

    openUpload=(e)=>{
       this.setState({open:true})
    }

    openImage=()=>{
        this.setState({image:true})
    }

    closeImage=()=>{
        this.setState({image:false})
    }

    render() {
        var re = /(?:\.([^.]+))?$/;
        // console.log("image", this.props.image)
        // console.log("image Type", this.props.imagePath.slice((this.props.imagePath.lastIndexOf(".") - 1 >>> 0) + 2))
        return (
            <div>
                <Segment>
                    {/* <div style={{width:'270px', height:'200px'}}> */}
                    <Image 
                    //  rounded
                     onClick={this.openImage}
                     verticalAlign='middle'
                    //  className='segmentImage'
                    //  style={{width:'200px', marginLeft:'auto', marginRight:'auto'}}
                    //  style={{objectFit: 'contain',width:'280px', height:'200px',maxWidth:'100%', display:'block'}}
                     label={{ as: 'a',size:'tiny',color: 'red', corner: 'right', icon: 'edit', onClick:this.openImage }}
                     size='medium'
                     ui={false}
                    //  wrapped={true}
                     src={
                         this.props.image!==null?
                        //  `data:image/${this.props.imagePath.slice((this.props.imagePath.lastIndexOf(".") - 1 >>> 0) + 2)};base64,${this.props.image}`
                         `data:image/${re.exec(this.props.imagePath)[1]};base64,${this.props.image}`
                        :'https://react.semantic-ui.com/images/wireframe/image.png'}
                     
                    />
                    {/* </div> */}
                    <Header>{this.props.name}</Header>
                    <Header color='orange' sub>From  {this.props.location}</Header>
                    <Divider/>
                    <Header>Update your CV below </Header>
                    {this.props.cvPath===null?
                   <Message error>
                    <Message.Header>Your CV</Message.Header>
                    <p>
                       Didn't you uplaod your CV to our system yet? Click below to upload
                    </p>
                    </Message>
                    :
                    <Label as='a' color='black' style={{marginBottom:'1em'}}>
                        File Available as
                        <Label.Detail>{this.props.cvPath}</Label.Detail>
                    </Label>
                    }
                    
                    <Button color='orange' onClick={this.openUpload} >Upload CV</Button>
                </Segment>
                <UploadCVModel open={this.state.open} close={this.closeUpload}/>
                <ImageUploader open={this.state.image} close={this.closeImage}/>
            </div>
        )
    }
}

export default UserProfileSegment
