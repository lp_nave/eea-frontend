import React, { Component } from 'react'
import { Modal, Input, Button, TransitionablePortal, Segment, Header, Form } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { CVActions } from '../../actions';

export class UploadCVModel extends Component {

    constructor(props){
        super(props);
        this.state={
            selectedFile:null,

            status:null,
            openPortal:false
        }
        this.onUpload = this.onUpload.bind(this)
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.isUploaded!==null){
            if(nextProps.isUploaded===true){
                return{
                    status:"success",
                    openPortal:true
                }
            }else if(nextProps.isUploaded===false){
                return{
                    status:"fail",
                    openPortal:true
                }
            }
        }
        else{
            return null
        }
    }

    async onUpload(e){
    //     let file = e.target.files[0]
        await this.setState({
            selectedFile: e.target.files[0]
        })
        // console.log("file", e.target.files[0], this.state.file)

    }

    handleUpload=()=>{
        const data = new FormData()
        data.append('file', this.state.selectedFile,`${localStorage.getItem("email")}CV.pdf`)
        console.log("file", this.state.selectedFile, data.getAll('file'))
        
        this.props.uploadCV(data)
    }

    handleClose=()=>{
        
        this.props.clearCV()
        this.setState({openPortal:false, status:null})
        this.props.close()
    }

    render() {
        console.log(this.state.file)
        return (
            <div>
                <Modal open={this.props.open} onClose={this.props.close}>
                    <Modal.Header>Upload your CV here</Modal.Header>
                    <Modal.Content>
                        <Form>
                            <Form.Input type='file' accept="application/pdf" onChange={this.onUpload}></Form.Input>
                        </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button disabled={this.state.selectedFile===null?true:false} color='orange' onClick={this.handleUpload}>Upload</Button>
                    </Modal.Actions>
                </Modal>
                <TransitionablePortal open={this.state.openPortal} onClose={this.handleClose} transition={{animation:"fly up", duration:500}}>
                        <Segment color='orange' style={{ backgroundColor:'lightgrey', left: '40%', position: 'fixed', bottom: '5%', zIndex: 1000 }}>
                        {this.state.status==="success"?
                            <>
                                <Header color='green'>Success!</Header>
                                <p>Successfully uploaded you CV </p>
                            </>
                        :null}
                        {this.state.status==='fail'?
                            <>
                                <Header color='red'>Failed!</Header>
                                <p>Failed to uploaded you CV </p>
                                <p>Please try again</p>
                            </>
                        :null}
                        <Button fluid onClick={this.handleClose} content="Close" negative/>
                        </Segment>
                </TransitionablePortal>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        isUploaded:state.CV.isUploaded,
        // companyData:state.Company.randomCompanies,
    }
}

export default connect(mapStateToProps,CVActions)(UploadCVModel)