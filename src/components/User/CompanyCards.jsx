import React, { Component } from 'react'
import { Card, Rating, Icon, Image, Header } from 'semantic-ui-react'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux'
import { CompanyActions } from '../../actions'

export class CompanyCards extends Component {

    constructor(props){
        super(props);
        
    }
    
    handleView=(id)=>{
        debugger
        //ISSUE wuth the withRouter 401 on jsx files
        //TEMP SOLUTION sent history from previous component
        // this.props.props.history.push({
        //     pathname:'/Company',
        //     data:id
        // })
        this.props.props.history.push(`/Company?id=${id}`);
        // this.props.viewCompany(id);
    }


    render() {
        
        console.log("cardprop",this.props.image)
        return (
            // <div style={{margin:'1em'}}>
                <Card raised onClick={()=>{this.handleView(this.props.id)}}>
                   <Image 
                     verticalAlign='middle'
                     onClick={this.openImage}
                     size={'tiny'}
                     style={{height:'200px',width:'200px', marginLeft:'auto', marginRight:'auto'}}
                     src={
                        this.props.image!==null?
                        `data:image/${this.props.imagePath.slice((this.props.imagePath.lastIndexOf(".") - 1 >>> 0) + 2)};base64,${this.props.image}`
                       :'https://react.semantic-ui.com/images/wireframe/image.png'}
                    wrapped ui={false}
                    />


                    <Card.Content>
                    <Card.Header>{this.props.name}</Card.Header>
                    <Card.Meta>
                        <Rating disabled icon='star' defaultRating={this.props.rating} maxRating={5}/>
                    </Card.Meta>
                    <Card.Description>
                    <Header color='orange' size='small'>{this.props.location}</Header>
                    <p>{`${this.props.description.substring(0,80)}...`}</p>
                    </Card.Description>
                    </Card.Content>
                    <Card.Content extra>
                        <Icon.Group>
                            {this.props.noOfJobs}
                            <br/>
                            jobs
                        </Icon.Group>
                    </Card.Content>  
                </Card>
            // </div>
        )
    }
}

// function mapStateToProps(state){
    //     return {}
    // }

export default CompanyCards


