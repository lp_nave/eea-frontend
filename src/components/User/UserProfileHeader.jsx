import React, { Component } from 'react'
import { Segment, Image, Header, Menu, Icon } from 'semantic-ui-react'
import '../../../src/image.css'

export class UserProfileHeader extends Component {

    constructor(props){
        super(props);

    }

    render() {
        return (
            <div style={{width:'90%', textAlign:'center', display:'inline-block'}}>
                <Segment
                 inverted
                //  textAlign='center'
                 style={{ 
                minHeight: 150, 
                // padding: '1em 0em',
                 width: "100%",
                //  height: '100%',
                //  display: 'inline-block',
                 backgroundImage: `url(${"/gradient.jpg"})`,
                 backgroundSize: 'cover',
                 marginBottom: 0,
                }}
                >  
                <Segment size='large' style={{
                    width: '150px',
                    height: '150px',
                    textAlign: 'center',
                    display: 'inline-block',
                    zIndex: 100,
                    top: '5em',
                    right: '32em',
                    padding:0
                }}>
                    <Image rounded size='medium' verticalAlign='middle'
                    // ui={false}
                    // wrapped={true}
                    style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        overflow: 'hidden',
                        flexShrink: 0,
                        minWidth: '100%',
                        minHeight: '100%',
                    }}
                    // className='image'
                        src={this.props.image!==null?
                            `data:image/${this.props.imagePath.slice((this.props.imagePath.lastIndexOf(".") - 1 >>> 0) + 2)};base64,${this.props.image}`
                           :'https://react.semantic-ui.com/images/wireframe/image.png'}
                        
                        />
                </Segment>
                </Segment>
                
                <Segment style={{marginTop:0}}>
                    <div style={{marginTop:'1.5em'}}>
                    <Header style={{marginLeft:'10em'}} floated='left'>{`${this.props.firstName} ${this.props.lastName}`}</Header>
                    <Header sub color='orange' floated='left'>{`${this.props.email}`}</Header>
                    {/* <div style={{float:'right'}}>
                        <Rating maxRating={5} defaultRating={3.5}/>
                        <Header floated='left' color='orange'>3.7</Header>
                    </div> */}
                    
                        <Menu inverted fluid borderless widths={4}>
                        <Menu.Item 
                           content={
                               <div>
                                   <Icon size='huge' name="briefcase"/>
                                    <Header color='orange' inverted style={{margin:0}}>{this.props.jobSize}</Header>
                                   <p >APPLIED JOBS</p>
                               </div>
                        }
                        />
                        {/* <Menu.Item
                            content={
                                <div>
                                    <Icon size='huge' name="clock"/>
                                    <Header color='orange' inverted style={{margin:0}}>3</Header>
                                    <p>PENDING</p>
                                </div>
                            }
                        /> */}
                        {/* <Menu.Item
                           content={
                                <div>
                                    <Icon size='huge' name="star half empty"/>
                                    <Header color='orange' inverted style={{margin:0}}>7</Header>
                                    <p>VACANCIES</p>
                                </div>
                            }
                        />
                        <Menu.Item
                           content={
                                <div>
                                    <Icon size='huge' name="factory"/>
                                    <Header color='orange' inverted style={{margin:0}}>4</Header>
                                    <p>DEPARTMENTS</p>
                                </div>
                            }
                        /> */}
                        </Menu>
                    </div>
                    
                    
                </Segment>
            </div>
        )
    }
}

export default UserProfileHeader
