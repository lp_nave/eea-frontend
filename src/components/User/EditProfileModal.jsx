import React, { Component } from 'react'
import { Modal, Form, Button, Header, Dimmer, Loader, TransitionablePortal, Segment, Label } from 'semantic-ui-react'
import EmployeeSignUp from '../EmployeeSignUp'
import { connect } from 'react-redux';
import { UserProfileActions } from '../../actions';

export class EditProfileModal extends Component {
    
    constructor(props){
        super(props);
        this.state={
            userData:null,
            apiState:false,

            email:'',
            firstName:'',
            lastName:'',
            contactNumber:'',
            location:'',
            password:'',
            rePassword:'',

            status:null,
            openPortal:false,
            error:false
        }
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.open===true && prevState.apiState ===false){
            debugger
            nextProps.getUser()
            return{
                apiState:true
            }
        }
        if(nextProps.userData && JSON.stringify(nextProps.userData)!== JSON.stringify(prevState.userData)){
            debugger
            return{
                userData:nextProps.userData,
                email:nextProps.userData.email,
                firstName:nextProps.userData.firstName,
                lastName:nextProps.userData.lastName,
                contactNumber:nextProps.userData.contactNumber,
                location:nextProps.userData.location,
            }
        }
        if(nextProps.isEdit!==null){
            if(nextProps.isEdit===true){
                return{
                    status:"success",
                    openPortal:true
                }
            }else if(nextProps.isEdit===false){
                return{
                    status:"fail",
                    openPortal:true
                }
            }
        }
        else{
            return null
        }
    }

    handleChange=(e,{value})=>{
        this.setState({
            [e.target.name]:value
        })
    }

    handleRePassword=(e,{value})=>{
        this.setState({
            rePassword:value
        })
    }

    hadndleEdit=()=>{
        if(this.state.password!==this.state.rePassword){
            this.setState({error:true})
        }else{
            this.props.editUser(this.state)
        }
    }

    
    handleClose=()=>{
        debugger
        this.props.clearUser()
        this.setState({openPortal:false, status:null})
        this.props.close()
    }

    render() {
        return (
            <Modal open={this.props.open} onClose={this.props.close} >
                <Modal.Header>Edit Profile</Modal.Header>
                <Modal.Content scrolling>
                    {this.props.userData?null:
                            <Dimmer active>
                                <Loader />
                            </Dimmer>
                        }
                    <Form size='large' onSubmit={this.hadndleEdit}>
                        
                        <Form.Input value={this.state.email} focus fluid icon='at' iconPosition='left' placeholder='E-mail address' />   
                        <Form.Input required value={this.state.firstName} name='firstName' onChange={this.handleChange} focus fluid placeholder='First Name' />
                        <Form.Input required value={this.state.lastName} name='lastName' onChange={this.handleChange}focus fluid placeholder='Last Name' />
                        <Form.Input required value ={this.state.location} name='location' onChange={this.handleChange}focus fluid placeholder='Location' />
                        <Form.Input required value={this.state.contactNumber} name='contactNumber' onChange={this.handleChange}focus fluid icon='phone' iconPosition='left' placeholder='Contact Number' />
                        <Header size='small' color='orange'>Fill inputs below if you wish to change your password </Header>
                        <Form.Input onChange={this.handleChange} value={this.state.password} name='password'  focus fluid icon='lock' iconPosition='left' placeholder='Password' type='password'/>
                        {this.state.error?
                                <Label prompt color='red' pointing='below'>
                                    The passwords do not match
                                </Label>
                            :null}
                        <Form.Input onChange={this.handleRePassword} focus fluid icon='lock' iconPosition='left' placeholder='Re enter Password' type='password'/>

                        <Button type='submit' color='orange' fluid size='large'>
                            Save Changes
                        </Button>
                    </Form>
                </Modal.Content>

                <TransitionablePortal open={this.state.openPortal} onClose={this.handleClose} transition={{animation:"fly up", duration:500}}>
                        <Segment color='orange' style={{  backgroundColor:'lightgrey',left: '40%', position: 'fixed', bottom: '5%', zIndex: 1000 }}>
                        {this.state.status==="success"?
                            <>
                                <Header color='green'>Success!</Header>
                                <p>Your Profile has been succesfully updated</p>
                            </>
                        :null}
                        {this.state.status==='fail'?
                            <>
                                <Header color='red'>Failed!</Header>
                                <p>Failed to upadate your profile.</p>
                                <p>Please try again</p>
                            </>
                        :null}
                        <Button fluid onClick={this.handleClose} content="Close" negative/>
                        </Segment>
                </TransitionablePortal>
                
            </Modal>
        )
    }
}

function mapStateToProps(state){
    return {
        ...state.UserProfile,
        userData:state.UserProfile.user,
        isEdit:state.UserProfile.isEdited,
        // companyData:state.Company.randomCompanies,
    }
}

export default connect(mapStateToProps,UserProfileActions)(EditProfileModal)
