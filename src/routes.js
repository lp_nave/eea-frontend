import Landingpage from './modules/Landingpage'
import Login from './modules/Login'
import SignUp from './modules/SignUp'
import UserHomePage from './modules/User/UserHomepage'
import Jobs from './modules/Jobs'
import CompanyHomePage from './modules/Company/CompanyHomepage'
import CompayProfile from './modules/Company/CompanyProfile'
import Applications from  './modules/Company/Applications'
import UserProfile from './modules/User/UserProfile';
import AllCompanies from './modules/Company/AllCompanies'
import Company from './modules/Company/Company';
import AdminPage from './modules/Admin/AdminPage';
import About from './modules/About'

const routes=[
    {
        path:'/About',
        exact:true,
        component:About
    },
    {
        path:'/Homepage',
        exact:true,
        component:Landingpage
    },
    {
        path:'/Login',
        exact:true,
        component:Login
    },
    {
        path:'/SignUp',
        exact:true,
        component:SignUp
    },
    {
        path:'/UserHomePage',
        exact:true,
        component:UserHomePage
    },
    {
        path:'/CompanyHomePage',
        exact:true,
        component:CompanyHomePage
    },
    {
        path:'/Jobs',
        exact:true,
        component:Jobs
    },
    {
        path:'/CompanyProfile',
        exact:true,
        component:CompayProfile
    },
    {
        path:'/Applicants',
        exact:true,
        component:Applications
    },
    {
        path:'/UserProfile',
        exact:true,
        component:UserProfile
    },
    {
        path:'/AllCompanies',
        exact:true,
        component:AllCompanies
    },
    {
        path:'/Admin',
        exact:true,
        component:AdminPage
    },
    {
        path:'/Company',
        exact:true,
        component:Company
    },
]

export default routes;
