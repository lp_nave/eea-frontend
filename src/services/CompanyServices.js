import {createLogic} from 'redux-logic'

import {CompanyActions,CompanyTypes, CompanyProfileActions} from "../actions"
import * as endPoints from './EndPoints';
import * as API from './HTTPClient';

const getRandomCompanies = createLogic({
    type:CompanyTypes.GET_RANDOM_COMPANIES,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API
        debugger

        HTTPClient.Get(endPoints.RANDOM_COMPANIES)
        .then(resp=>{
            dispatch(CompanyActions.getRandomCompaniesSuccess(resp.data))
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CompanyActions.getRandomCompaniesFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const addDepartment = createLogic({
    type:CompanyTypes.ADD_DEPARTMENT,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let obj={
            departmentName:action.payload,
            email: localStorage.getItem("email")
        }
        console.log("department add", obj);
        debugger

        HTTPClient.Post(endPoints.ADD_DEPARTMENT, obj)
        .then(resp=>{
            dispatch(CompanyActions.addDepartmentSuccess(resp.data))
            dispatch(CompanyProfileActions.getCompanySegment())
            dispatch(CompanyActions.getAllDepartments())
        })
        .catch(err=>{
            var errormsg="Failed to add Department";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CompanyActions.addDepartmentFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const getDepartments = createLogic({
    type:CompanyTypes.GET_ALL_DEPARTMENTS,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let email = localStorage.getItem("email")

        debugger

        HTTPClient.Get(endPoints.GET_ALL_DEPARTMENTS + `/${email}` )
        .then(resp=>{
            dispatch(CompanyActions.getAllDepartmentsSuccess(resp.data))
        })
        .catch(err=>{
            var errormsg="Failed to Departments";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CompanyActions.getAllDepartmentsFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const deleteDepartment = createLogic({
    type:CompanyTypes.DELETE_DEPARTMENT,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let id = action.payload

        debugger

        HTTPClient.Delete(endPoints.DELETE_DEPARTMENT + `/${id}` )
        .then(resp=>{
            dispatch(CompanyActions.deleteDepartmentSuccess(resp.data))
            dispatch(CompanyActions.getAllDepartments())
            dispatch(CompanyProfileActions.getCompanyProfile())
        })
        .catch(err=>{
            var errormsg="Failed to Departments";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CompanyActions.deleteDepartmentFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const getAllCompanies = createLogic({
    type:CompanyTypes.GET_ALL_COMPANIES,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        debugger

        HTTPClient.Get(endPoints.GET_ALL_COMPANIES)
        .then(resp=>{
            dispatch(CompanyActions.getAllCompaniesSuccess(resp.data))
            // dispatch(CompanyActions.getAllDepartments())
        })
        .catch(err=>{
            var errormsg="Failed to Departments";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CompanyActions.getAllCompaniesFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const searchCompanies = createLogic({
    type:CompanyTypes.SEARCH_COMPANIES,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let search=action.payload.searchkey
        let location= action.payload.location
        debugger

        let query
        if(search===null){
            query=`?loc=${location}`
        }else if(location===null){
            query=`?key=${search}`
        }else{
            query=`?key=${search}&loc=${location}`
        }

        HTTPClient.Get(endPoints.SEARCH_COMPANIES+ query)
        .then(resp=>{
            dispatch(CompanyActions.searchCompaniesSuccess(resp.data))
            // dispatch(CompanyActions.getAllDepartments())
        })
        .catch(err=>{
            var errormsg="Failed to get Search results";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CompanyActions.searchCompaniesFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const viewCompany = createLogic({
    type:CompanyTypes.VIEW_COMPANY,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let id=action.payload
        debugger

        HTTPClient.Get(endPoints.VIEW_COMPANY+ `/${id}`)
        .then(resp=>{
            dispatch(CompanyActions.viewCompanySuccess(resp.data))
            // dispatch(CompanyActions.getAllDepartments())
        })
        .catch(err=>{
            var errormsg="Failed to get Search results";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CompanyActions.viewCompanyFail(errormsg))
        })
        .then(()=>{done()});
    }
})

export default [
    getRandomCompanies,
    addDepartment,
    getDepartments,
    deleteDepartment,
    getAllCompanies,
    searchCompanies,
    viewCompany
]