import {createLogic} from 'redux-logic'

import {AdminTypes, AdminActions} from "../actions"
import * as endPoints from './EndPoints';
import * as API from './HTTPClient';

const getAllCompanies = createLogic({
    type:AdminTypes.ADMIN_GET_COMPANIES,
    latest:true,
    debounce:1000,

    process({action}, dispatch,done){
        let HTTPClient = API

        HTTPClient.Get(endPoints.GET_ADMIN_COMPANIES)
        .then(resp=>{
            dispatch(AdminActions.adminCompaniesSuccess(resp.data));
            debugger
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(AdminActions.adminCompaniesFail(errormsg))
        })
        

    }
})

const getAllJobs = createLogic({
    type:AdminTypes.ADMIN_GET_JOBS,
    latest:true,
    debounce:1000,

    process({action}, dispatch,done){
        let HTTPClient = API
        debugger

        HTTPClient.Get(endPoints.GET_ADMIN_JOBS)
        .then(resp=>{
            dispatch(AdminActions.adminJobsSuccess(resp.data));
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(AdminActions.adminJobsFail(errormsg))
        })

    }
})

const getAllClients = createLogic({
    type:AdminTypes.ADMIN_GET_CLIENTS,
    latest:true,
    debounce:1000,

    process({action}, dispatch,done){
        let HTTPClient = API

        HTTPClient.Get(endPoints.GET_ADMIN_CLIENTS)
        .then(resp=>{
            dispatch(AdminActions.adminClientsSuccess(resp.data));
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(AdminActions.adminClientsFail(errormsg))
        })

    }
})

const downloadAllClients = createLogic({
    type:AdminTypes.ADMIN_DOWNLOAD_CLIENTS,
    latest:true,
    debounce:1000,

    process({action}, dispatch,done){
        let HTTPClient = API

        HTTPClient.Get(endPoints.DOWNLOAD_CLIENTS)
        .then((resp)=>{
            return resp.data
        })
        .then(data=>{
            var link = document.createElement('a');

                link.href = window.URL.createObjectURL(
                    // new Blob([data], {type:"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"})
                    new Blob([data], {type:"application/csv;charset=utf-8"})
                  );

                var today = new Date();
                var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + ":" + today.getMilliseconds();
                var dateTime = date + ' ' + time;
                var fileName = dateTime + " Client report.csv";

                link.download = fileName;
                link.target ="_blank";
                link.click();
        })
        .catch(err=>{
            alert("Error Downloading Client details")
        })
        .then(done());

    }
})

const downloadAllCompanies = createLogic({
    type:AdminTypes.ADMIN_DOWNLOAD_COMPANIES,
    latest:true,
    debounce:1000,

    process({action}, dispatch,done){
        let HTTPClient = API

        HTTPClient.Get(endPoints.DOWNLOAD_COMPANIES)
        .then((resp)=>{
            return resp.data
        })
        .then(data=>{
            var link = document.createElement('a');

                link.href = window.URL.createObjectURL(
                    // new Blob([data], {type:"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"})
                    new Blob([data], {type:"application/csv;charset=utf-8"})
                  );

                var today = new Date();
                var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + ":" + today.getMilliseconds();
                var dateTime = date + ' ' + time;
                var fileName = dateTime + " Company report.csv";

                link.download = fileName;
                link.target ="_blank";
                link.click();
        })
        .catch(err=>{
            alert("Error Downloading Client details")
        })
        .then(done());

    }
})
const downloadAllJobs = createLogic({
    type:AdminTypes.ADMIN_DOWNLOAD_JOBS,
    latest:true,
    debounce:1000,

    process({action}, dispatch,done){
        let HTTPClient = API
        debugger

        HTTPClient.Get(endPoints.DOWNLOAD_JOBS)
        .then((resp)=>{
            return resp.data
        })
        .then(data=>{
            var link = document.createElement('a');

                link.href = window.URL.createObjectURL(
                    // new Blob([data], {type:"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"})
                    new Blob([data], {type:"application/csv;charset=utf-8"})
                  );

                var today = new Date();
                var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + ":" + today.getMilliseconds();
                var dateTime = date + ' ' + time;
                var fileName = dateTime + " Job report.csv";

                link.download = fileName;
                link.target ="_blank";
                link.click();
        })
        .catch(err=>{
            alert("Error Downloading Client details")
        })
        .then(done());

    }
})

const deleteCompany = createLogic({
    type:AdminTypes.DELETE_COMPANY,
    latest:true,
    debounce:1000,

    process({action}, dispatch,done){
        let HTTPClient = API
        debugger

        HTTPClient.Delete(endPoints.ADMIN_DELETE_COMPANY+`/${action.payload}`)
        .then((resp)=>{
            dispatch(AdminActions.adminCompanies())
            debugger
        })
        .catch(err=>{
            alert("Error deleting company")
        })
    }
})

const deleteClient = createLogic({
    type:AdminTypes.DELETE_CLIENT,
    latest:true,
    debounce:1000,

    process({action}, dispatch,done){
        let HTTPClient = API
        debugger

        HTTPClient.Delete(endPoints.ADMIN_DELETE_CLIENT+`/${action.payload}`)
        .then((resp)=>{
            dispatch(AdminActions.adminClients())
        })
        .catch(err=>{
            alert("Error deleting client")
        })

    }
})

const deleteJob = createLogic({
    type:AdminTypes.DELETE_JOBS,
    latest:true,
    debounce:1000,

    process({action}, dispatch,done){
        let HTTPClient = API
        debugger

        HTTPClient.Delete(endPoints.ADMIN_DELETE_JOB +`/${action.payload}`)
        .then((resp)=>{
            dispatch(AdminActions.adminJobs())
            debugger
        })
        .catch(err=>{
            alert("Error Deleting Job")
        })

    }
})

export default [
    getAllCompanies,
    getAllJobs,
    getAllClients,
    downloadAllClients,
    downloadAllCompanies,
    downloadAllJobs,
    deleteCompany,
    deleteClient,
    deleteJob

]