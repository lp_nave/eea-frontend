import axios from 'axios'

// TODO: Replace this with actual JWT token from Keycloak
const id_token = "secret";
axios.defaults.headers.post['Content-Type'] = 'application/json';
// Create axios instance for api calls
var instance = null;

export const setAuth = () => {

    if(localStorage.jwt == undefined){
        instance = axios.create({
            baseURL: '',
            timeout: 30000,
    
        }
        )
    }else{
        instance = axios.create({
            baseURL: '',
            timeout: 30000,
    
            headers: {
                'Authorization': 'Bearer ' + localStorage.jwt,
                'Content-Type': 'application/json'
            }
        }
        )
    }
    
    // instance = axios.create({
    //     baseURL: '',
    //     timeout: 30000,

    //     headers: {
    //         'Authorization': 'Bearer ' + localStorage.jwt,
    //         'Content-Type': 'application/json'
    //     }
    // }
    // )

    instance.interceptors.response.use(function (response) {
        return response;
    }, function (error) {
        //add a handler for 500 internal server error
        if (error.response.status === 401) {
            localStorage.removeItem('jwt');
            localStorage.removeItem('user');
            localStorage.removeItem('email');
            if(window.location.pathname!=='/Login'){
                window.location = '/Login'
            }
        }
        else {
            return Promise.reject(error);
        }
    });
}

export const Get=(route, data)=>{
    instance|| setAuth()

    return instance.get(route,data)
}

export const Post = (route, data) => {
    instance || setAuth()
    return instance.post(route, JSON.stringify(data))
}
export const PostPDF = (route,data, responseType='blob') => {
    instance || setAuth()
    return instance.post(route,data,responseType)
}
export const Put = (route, data) => {
    instance || setAuth()
    return instance.put(route, JSON.stringify(data))
    // return instance.put(route, data)
    // return instance.put(route, data == {} ? null : JSON.stringify(data))
}

export const Delete = (route, data) => {
    instance || setAuth()
    return instance.delete(route, JSON.stringify(data))
}