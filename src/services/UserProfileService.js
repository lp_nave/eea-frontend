import {createLogic} from 'redux-logic'

import {UserProfileActions,UserProfileTypes} from "../actions"
import * as endPoints from './EndPoints';
import * as API from './HTTPClient';

const getUserProfileSegment = createLogic({
    type:UserProfileTypes.GET_USER_USER_PROFILE_SEGMENT,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let email= localStorage.getItem('email')
        console.log("check", email)
        debugger

        HTTPClient.Get(endPoints.GET_USER_PROFILE_SEGMENT + `/${email}`)
        .then(resp=>{
            dispatch(UserProfileActions.getUserProfileSegmentSuccess(resp.data))
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(UserProfileActions.getUserProfileSegmentFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const getUserProfile = createLogic({
    type:UserProfileTypes.GET_USER_PROFILE,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let email= localStorage.getItem('email')
        console.log("check", email)
        debugger

        HTTPClient.Get(endPoints.GET_USER_PROFILE + `/${email}`)
        .then(resp=>{
            dispatch(UserProfileActions.getUserProfileSuccess(resp.data))
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(UserProfileActions.getUserProfileFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const getUser = createLogic({
    type:UserProfileTypes.GET_USER,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let email= localStorage.getItem('email')
        console.log("check", email)
        debugger

        HTTPClient.Get(endPoints.GET_USER + `/${email}`)
        .then(resp=>{
            dispatch(UserProfileActions.getUserSuccess(resp.data))
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(UserProfileActions.getUserFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const editUser = createLogic({
    type:UserProfileTypes.EDIT_USER,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let obj={
            email:action.payload.email,
            firstName:action.payload.firstName,
            lastName:action.payload.lastName,
            contactNumber:action.payload.contactNumber,
            location:action.payload.location,
            password:action.payload.password
        }
        debugger

        HTTPClient.Put(endPoints.EDIT_USER ,obj)
        .then(resp=>{
            dispatch(UserProfileActions.editUserSuccess(resp.data))
            dispatch(UserProfileActions.getUserProfile())
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(UserProfileActions.editUserFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const applyForJob = createLogic({
    type:UserProfileTypes.APPLY_FOR_JOB,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let obj={
            jobId:action.payload,
            email:localStorage.getItem('email'),
        }
        debugger

        HTTPClient.Post(endPoints.APPLY_FOR_JOB ,obj)
        .then(resp=>{
            dispatch(UserProfileActions.applyForJobSuccess(resp.data))
            // dispatch(UserProfileActions.getUserProfile())
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(UserProfileActions.applyForJobFail(errormsg))
        })
        .then(()=>{done()});
    }
})

export default [
    getUserProfileSegment,
    getUserProfile,
    getUser,
    editUser,
    applyForJob
]