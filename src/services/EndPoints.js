//Authentication
export const SIGNUPCLIENT = "/registerClient";
export const SIGNUPCOMPANY = "/registerCompany";
export const AUTHENTICATE ="/authenticate";

//Client
export const GET_USER_PROFILE_SEGMENT = "/clientProfileSegment";
export const GET_USER_PROFILE ='/clientProfile';
export const GET_USER ='/getClient';
export const EDIT_USER ='/editClientProfile';
export const APPLY_FOR_JOB ='/applyForJob';

//General
export const RANDOM_COMPANIES = "/getRandomCompanies";
export const GET_ALL_COMPANIES="/getCompanies"
export const VIEW_COMPANY ="/viewCompany"

//Company
export const GET_COMPANY_PROFILE_SEGMENT ="/companySegment";
export const GET_COMPANY_PROFILE ="/getCompanyProfile";
export const GET_COMPANY ="/getCompany";
export const EDIT_COMPANY ='/editCompany';
export const GET_APPLICANTS="/getApplicants"
export const RATE="/setRating";

//Departments
export const ADD_DEPARTMENT ="/addDepartment";
export const GET_ALL_DEPARTMENTS ="/getAllDepartments";
export const DELETE_DEPARTMENT ='/deleteDepartment'

// Jobs
export const CREATE_JOB ="/addJob";
export const GET_JOB ='/getJob';
export const EDIT_JOB ='/editJob';
export const DELETE_JOB ='/deleteJob';
export const GET_ALL_JOBS ='/getAllJobs';

//CV
export const UPLOAD_CV='/uploadCV';
export const DOWNLOAD_CV='/getCV';

//IMages
export const UPLOAD_IMAGE='/uploadImage'

//Search
export const SEARCH_COMPANIES='/searchCompany'
export const SEARCH_JOBS='/searchJobs'

// Admin
export const GET_ADMIN_COMPANIES ='/adminCompanies'
export const GET_ADMIN_JOBS ='/adminJobs'
export const GET_ADMIN_CLIENTS ='/adminClients'

export const ADMIN_DELETE_JOB='/DeleteJob'
export const ADMIN_DELETE_COMPANY ='DeleteCompany'
export const ADMIN_DELETE_CLIENT ='/DeleteClient'


//Download
export const DOWNLOAD_CLIENTS ="/ClientsCSV"
export const DOWNLOAD_COMPANIES ="/CompanyCSV"
export const DOWNLOAD_JOBS ='/JobCSV'