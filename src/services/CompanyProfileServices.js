import {createLogic} from 'redux-logic'

import {CompanyProfileActions,CompanyProfileTypes} from "../actions"
import * as endPoints from './EndPoints';
import * as API from './HTTPClient';

const getCompanyProfileSegment = createLogic({
    type:CompanyProfileTypes.GET_COMPANY_SEGMENT,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let email= localStorage.getItem('email')
        console.log("check", email)
        debugger

        HTTPClient.Get(endPoints.GET_COMPANY_PROFILE_SEGMENT + `/${email}`)
        .then(resp=>{
            dispatch(CompanyProfileActions.getCompanySegmentSuccess(resp.data))
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CompanyProfileActions.getCompanySegmentFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const getCompanyProfile = createLogic({
    type:CompanyProfileTypes.GET_COMPANY_PROFILE,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let email= localStorage.getItem('email')
        console.log("check", email)
        debugger

        HTTPClient.Get(endPoints.GET_COMPANY_PROFILE + `/${email}`)
        .then(resp=>{
            dispatch(CompanyProfileActions.getCompanyProfileSuccess(resp.data))
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CompanyProfileActions.getCompanyProfileFail(errormsg))
        })
        .then(()=>{done()});
    }
})
const getCompany = createLogic({
    type:CompanyProfileTypes.GET_COMPANY,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let email= localStorage.getItem('email')
        console.log("check", email)
        debugger

        HTTPClient.Get(endPoints.GET_COMPANY + `/${email}`)
        .then(resp=>{
            dispatch(CompanyProfileActions.getCompanySuccess(resp.data))
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CompanyProfileActions.getCompanyFail(errormsg))
        })
        .then(()=>{done()});
    }
})
const editCompany = createLogic({
    type:CompanyProfileTypes.EDIT_COMPANY,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let obj ={
            email:action.payload.email,
            companyName:action.payload.companyName,
            address:action.payload.address,
            contactNumber: action.payload.contactNumber,
            noOfEmployees: action.payload.noOfEmployees,
            noOfJobs: action.payload.noOfJobs,
            description: action.payload.description,
            location: action.payload.location,
            password: action.payload.password
        }
        console.log("check", obj)
        debugger

        HTTPClient.Put(endPoints.EDIT_COMPANY, obj)
        .then(resp=>{
            dispatch(CompanyProfileActions.editCompanySuccess(resp.data))
            dispatch(CompanyProfileActions.getCompanyProfile())
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CompanyProfileActions.editCompanyFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const getApplicants = createLogic({
    type:CompanyProfileTypes.GET_APPLICANTS,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let email = localStorage.getItem("email")
        debugger

        HTTPClient.Get(endPoints.GET_APPLICANTS + `/${email}`)
        .then(resp=>{
            dispatch(CompanyProfileActions.getApplicantsSuccess(resp.data))
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CompanyProfileActions.getApplicantsFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const rate = createLogic({
    type:CompanyProfileTypes.RATE,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

    //    let obj={
    //         companyID:action.payload.id,
    //         noOfStars:action.payload.rate
    //    }
        let companyID=action.payload.id
        let noOfStars=action.payload.rate
       
        debugger

        // HTTPClient.Post(endPoints.RATE, obj)
        HTTPClient.Get(endPoints.RATE + `/${companyID}/${noOfStars}`)
        .then(resp=>{
            dispatch(CompanyProfileActions.rateSuccess(resp.data))
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CompanyProfileActions.rateFail(errormsg))
        })
        .then(()=>{done()});
    }
})

export default [
    getCompanyProfileSegment,
    getCompanyProfile,
    getCompany,
    editCompany,
    getApplicants,
    rate
]