
import AuthServices from './AuthServices'
import UserProfileService from './UserProfileService'
import CompanyServices from './CompanyServices'
import CompanyProfileServices from './CompanyProfileServices'
import JobService from './JobService'
import CVService from './CVService'
import AdminServices from './AdminServices'

export default [
    ...AuthServices,
    ...UserProfileService,
    ...CompanyServices,
    ...CompanyProfileServices,
    ...JobService,
    ...CVService,
    ...AdminServices
]