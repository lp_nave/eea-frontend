import {createLogic} from 'redux-logic'

import {JobActions,JobTypes, CompanyProfileActions} from "../actions"
import * as endPoints from './EndPoints';
import * as API from './HTTPClient';

const createJob = createLogic({
    type:JobTypes.CREATE_JOB,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let obj={
            name:action.payload.jobTitle,
            description:action.payload.description,
            requirements:action.payload.requirements,
            type:action.payload.type,
            salary:action.payload.salary,
            deptId:action.payload.department,
            location:action.payload.location
        }
        debugger

        HTTPClient.Post(endPoints.CREATE_JOB , obj)
        .then(resp=>{
            dispatch(JobActions.createJobSuccess(resp.data))
            dispatch(CompanyProfileActions.getCompanySegment())
            dispatch(CompanyProfileActions.getCompanyProfile())
        })
        .catch(err=>{
            var errormsg="Failed to create Job";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(JobActions.createJobFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const editJob = createLogic({
    type:JobTypes.EDIT_JOB,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let obj={
            jobId:action.payload.id,
            name:action.payload.jobTitle,
            description:action.payload.description,
            requirements:action.payload.requirements,
            type:action.payload.type,
            salary:action.payload.salary,
            // deptId:action.payload.department,
            location:action.payload.location
        }
        debugger

        HTTPClient.Put(endPoints.EDIT_JOB , obj)
        .then(resp=>{
            dispatch(JobActions.editJobSuccess(resp.data))
            dispatch(CompanyProfileActions.getCompanyProfile())
        })
        .catch(err=>{
            var errormsg="Failed to create Job";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(JobActions.editJobFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const getJob = createLogic({
    type:JobTypes.GET_JOB,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let id=action.payload
        debugger

        HTTPClient.Get(endPoints.GET_JOB +`/${id}`)
        .then(resp=>{
            debugger
            dispatch(JobActions.getJobSuccess(resp.data))
        })
        .catch(err=>{
            var errormsg="Failed to create Job";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(JobActions.getJobFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const deleteJob = createLogic({
    type:JobTypes.DELETE_JOB,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let id=action.payload
        debugger

        HTTPClient.Delete(endPoints.DELETE_JOB +`/${id}`)
        .then(resp=>{
            debugger
            dispatch(JobActions.deleteJobSuccess(resp.data))
            dispatch(CompanyProfileActions.getCompanyProfile())
        })
        .catch(err=>{
            var errormsg="Failed to create Job";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(JobActions.deleteJobFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const getAllJobs = createLogic({
    type:JobTypes.GET_ALL_JOBS,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        debugger

        HTTPClient.Get(endPoints.GET_ALL_JOBS)
        .then(resp=>{
            debugger
            dispatch(JobActions.getAllJobsSuccess(resp.data))
            // dispatch(CompanyProfileActions.getCompanyProfile())
        })
        .catch(err=>{
            var errormsg="Failed to create Job";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(JobActions.getAllJobsFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const searchJobs = createLogic({
    type:JobTypes.SEARCH_JOBS,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let search=action.payload.searchkey
        let location= action.payload.location

        debugger

        let query
        if(search===null){
            query=`?loc=${location}`
        }else if(location===null){
            query=`?key=${search}`
        }else{
            query=`?key=${search}&loc=${location}`
        }

        HTTPClient.Get(endPoints.SEARCH_JOBS + query)
        .then(resp=>{
            debugger
            dispatch(JobActions.searchJobsSuccess(resp.data))
            // dispatch(CompanyProfileActions.getCompanyProfile())
        })
        .catch(err=>{
            var errormsg="Failed to create Job";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(JobActions.searchJobsFail(errormsg))
        })
        .then(()=>{done()});
    }
})

export default [
    createJob,
    getJob,
    editJob,
    deleteJob,
    getAllJobs,
    searchJobs
]