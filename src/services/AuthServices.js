import {createLogic} from 'redux-logic'

import {authActions, authTypes} from "../actions"
import jwtDecode from 'jwt-decode';
import * as endPoints from './EndPoints';
import * as API from './HTTPClient';

const signupClient = createLogic({
    type:authTypes.REGISTER_CLIENT,
    latest:true,
    debounce:1000,

    process({
        action
    }, dispatch,done){

        let HTTPClient = API

        // let obj={
        //     firstName:"Lahiru",
        //     lastName:"Navaratana",
        //     email:"lp_nave",
        //     nic: "02938209384",
        //     drivingLicenseNumber:"023780298538",
        //     dob:"1996-02-05",
        //     contactNumber:"0713031690",
        //     password:"javainuse"
        // }
        let obj={
            firstName:action.payload.firstName,
            lastName:action.payload.lastName,
            email:action.payload.email,
            contactNumber:action.payload.contactNumber,
            location:action.payload.location,
            password:action.payload.password
        }
        console.log("obj", obj)
        debugger

        HTTPClient.Post(endPoints.SIGNUPCLIENT , obj)
            .then(resp=> {
                debugger 
                if(resp.data=="User already exists"){
                    debugger
                    dispatch(authActions.registerClientFail(resp.data))
                }else{
                    let decodedToken = jwtDecode(resp.data.jwtToken);
                    console.log("decoded", decodedToken)
                    debugger
                    localStorage.setItem("jwt",resp.data.jwtToken);
                    localStorage.setItem("role",decodedToken.role);
                    localStorage.setItem("email",decodedToken.sub);
                    HTTPClient.setAuth();
                    dispatch(authActions.registerClientSuccess(resp.data))
                }
            })
            .catch(err=>{
                var errormsg="Failed to Register";
                if (err && err.code == "ECONNABORTED") {
                    errormsg = "Please check your internet connection.";
                }
                debugger
                dispatch(authActions.registerClientFail(errormsg))
            })
            .then(() => done());
    }
})

const signupCompany = createLogic({
    type:authTypes.REGISTER_COMPANY,
    latest:true,
    debounce:1000,

    process({
        action
    }, dispatch,done){

        let HTTPClient = API

        // let obj={
        //     firstName:"Lahiru",
        //     lastName:"Navaratana",
        //     email:"lp_nave",
        //     location:"kandy",
        //     contactNumber:"0713031690",
        //     password:"javainuse"
        // }
        let obj={
            email:action.payload.email,
            companyName:action.payload.companyName,
            address:action.payload.address,
            contactNumber:action.payload.contactNumber,
            description:action.payload.description,
            noOfEmployees:action.payload.noOfEmployees,
            noOfJobs:action.payload.noOfJobs,
            location:action.payload.location,
            password:action.payload.password
        }
        console.log("obj", obj, action.payload)
        debugger

        HTTPClient.Post(endPoints.SIGNUPCOMPANY , obj)
            .then(resp=> {
                if(resp.data=="Company already exists"){
                    debugger
                    dispatch(authActions.registerCompanyFail(resp.data))
                }else{
                    let decodedToken = jwtDecode(resp.data.jwtToken);
                    console.log("decoded", decodedToken)
                    debugger
                    localStorage.setItem("jwt",resp.data.jwtToken);
                    localStorage.setItem("role",decodedToken.role);
                    localStorage.setItem("email",decodedToken.sub);
                    HTTPClient.setAuth();
                    dispatch(authActions.registerCompanySuccess(resp.data))
                }
            })
            .catch(err=>{
                var errormsg="Failed to Register";
                if (err && err.code == "ECONNABORTED") {
                    errormsg = "Please check your internet connection.";
                }
                dispatch(authActions.registerCompanyFail(errormsg))
            })
            .then(() => done());
    }
})
const authenticate = createLogic({
    type:authTypes.AUTHENTICATE,
    latest:true,
    debounce:1000,

    process({
        action
    }, dispatch,done){

        let HTTPClient = API

        let obj ={
            email:action.payload.email,
            password: action.payload.password
        }
       
        HTTPClient.Post(endPoints.AUTHENTICATE , obj)
            // .then(resp=> resp.data)
            .then(resp=>{
                let decodedToken = jwtDecode(resp.data.jwtToken);
                console.log("decoded", decodedToken)
                debugger
                localStorage.setItem("jwt",resp.data.jwtToken);
                localStorage.setItem("role",decodedToken.role);
                localStorage.setItem("email",decodedToken.sub);
                HTTPClient.setAuth();
                dispatch(authActions.authenticateSuccess(resp.data))
            })
            .catch(err=>{
                debugger
                var errormsg="Failed to Login";
                if (err && err.code == "ECONNABORTED") {
                    errormsg = "Please check your internet connection.";
                }
                dispatch(authActions.authenticateFail(errormsg))
            })
            .then(() => done());
    }
})

export default [
    signupClient,
    signupCompany,
    authenticate,
]