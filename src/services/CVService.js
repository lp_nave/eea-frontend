import {createLogic} from 'redux-logic'

import {CVActions,CVTypes, UserProfileActions, CompanyProfileActions} from "../actions"
import * as endPoints from './EndPoints';
import * as APIU from './UploadClient';
import * as API from './HTTPClient';

const uploadCV = createLogic({
    type:CVTypes.UPLOAD_CV,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = APIU

        let email= localStorage.getItem('email')
        let obj=action.payload
        console.log("check", email)
        debugger

        HTTPClient.Post(endPoints.UPLOAD_CV + `/${email}`, obj)
        .then(resp=>{
            debugger
            dispatch(CVActions.uploadCVSuccess(resp.data))
            dispatch(UserProfileActions.getUserProfileSegment())
            // dispatch(UserProfileActions.getUserProfileSegment())
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CVActions.uploadCVFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const downloadCV = createLogic({
    type:CVTypes.DOWNLOAD_CV,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = API

        let email= action.payload
        // let obj=action.payload
        console.log("check", email)
        debugger

        HTTPClient.Get(endPoints.DOWNLOAD_CV + `/${email}`, {responseType:"blob"})
        // .the(resp=>{
        //     return resp.data
        // })
        .then(resp=> {
            debugger
            console.log(resp.data)
            var blob = new Blob([resp.data], {type:"application/pdf"})
            var file = window.URL.createObjectURL(blob);
            debugger
            var link = document.createElement('a');
            link.href= file

            link.download = email+'.pdf';
            // link.target ="_blank";
            link.click();
            
        })

        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CVActions.downloadCVFail(errormsg))
        })
        .then(()=>{done()});
    }
})

const uploadImage = createLogic({
    type:CVTypes.UPLOAD_IMAGE,
    latest:true,
    debounce: 1000,

    process({action},dispatch, done){
        let HTTPClient = APIU

        let email= localStorage.getItem('email')
        let role = localStorage.getItem('role')
        let obj=action.payload
        console.log("check", email)
        debugger

        HTTPClient.Post(endPoints.UPLOAD_IMAGE + `/${email}/${role}`, obj)
        .then(resp=>{
            debugger
            dispatch(CVActions.uploadImageSuccess(resp.data))
            if(role=='client'){
                dispatch(UserProfileActions.getUserProfileSegment())
            }
            else{
                dispatch(CompanyProfileActions.getCompanySegment())
            }
            // dispatch(UserProfileActions.getUserProfileSegment())
        })
        .catch(err=>{
            var errormsg="Failed to get data";
            if (err && err.code == "ECONNABORTED") {
                errormsg = "Please check your internet connection.";
            }
            dispatch(CVActions.uploadImageFail(errormsg))
        })
        .then(()=>{done()});
    }
})


export default [
    uploadCV,
    downloadCV,
    uploadImage
]